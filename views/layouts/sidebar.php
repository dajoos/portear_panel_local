<?php

use app\models\Locals;
use hail812\adminlte3\widgets\Menu;
use yii\helpers\Url;

?>
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <a href="<?= Url::home() ?>" class="brand-link">
        <img src="<?= Yii::getAlias('@web') . '/img/logo-white.png' ?>" alt="Logo" class="brand-image ml-1 mr-1">
        <span class="brand-text font-weight-light ml-2">Panel de control</span>
    </a>
    <div class="sidebar">
        <div class="user-panel mt-3 pb-3 mb-3">
            <?php
            try {
                echo Menu::widget([
                    'items' => [
                        [
                            'label' => Yii::$app->user->identity->fullname,
                            'icon' => 'user',
                            'url' => ['site/profile']
                        ],
                    ]
                ]);
            } catch (Exception $e) {
            }
            ?>
        </div>
        <nav class="mt-2">
            <?php

            $items = [];
            $i = 0;
            foreach (Locals::findByOwner() as $value) {
                $items[$i] = [
                    'label' => $value['name'],
                    'icon' => 'laptop-house',
                    'url' => ['local/index?id=' . $value['_id']],
                    'active' => isset($_GET['id']) && $_GET['id'] == $value['_id'] //$this->context->route == 'site/projects'
                ];
                $i++;
            }

            try {
                echo Menu::widget([
                    'items' => [
                        [
                            'label' => 'Inicio',
                            'icon' => 'home',
                            'url' => ['site/index']
                        ],
                        [
                            'label' => 'Mis locales',
                            'icon' => 'building',
                            'items' => $items,
                        ],
                        ['label' => 'Otros', 'header' => true],
                        [
                            'label' => 'Contactos',
                            'icon' => 'phone',
                            'url' => ['site/contact']
                        ],
                        [
                            'label' => 'Acerca de nosotros',
                            'icon' => 'address-card',
                            'url' => ['site/about']
                        ],
                        [
                            'label' => 'Términos y condiciones',
                            'icon' => 'file-signature',
                            'url' => ['site/tnc']
                        ],
                    ],
                ]);
            } catch (Exception $e) {
            }
            ?>
        </nav>
    </div>
</aside>