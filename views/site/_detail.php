<?php

use app\models\DeliveryBoys;
use app\models\Items;
use app\models\Orders;
use kartik\grid\GridView;
use yii\data\ArrayDataProvider;

/* @var $model Orders */
/* @var $dataProvider ArrayDataProvider */

//$before = false;
//if ($model["status"] == '0') {
//    $urlReject = Yii::$app->getUrlManager()->createAbsoluteUrl(['site/reject', 'id' => $model["_id"]->__toString()]);
//    $before = HTML::button('<i class="fas fa-check"></i>', ArrayHelper::merge(['value' => $model["_id"]->__toString()], ['class' => 'btn btn-success _accept'], ['title' => Yii::t('app', 'Aceptar')])) . ' ' . HTML::button('<i class="fas fa-times"></i>', ArrayHelper::merge(['value' => $urlReject], ['class' => 'btn btn-danger _modalButton'], ['title' => Yii::t('app', 'Rechazar')]));
//} else if ($model["status"] == '1') {
//    $before = HTML::button('<i class="fas fa-user-plus"></i>', ArrayHelper::merge(['value' => $model["_id"]->__toString()], ['class' => 'btn btn-primary _assign'], ['title' => Yii::t('app', 'Asignar')]));// . ' ' . HTML::button('<i class="fas fa-times"></i>', ArrayHelper::merge(['value' => $model["_id"]->__toString()], ['class' => 'btn btn-danger _modalButton'], ['title' => Yii::t('app', 'Rechazar')]));
//} else if ($model["status"] == '3')
//    $before = HTML::button('<i class="fas fa-spinner"></i>', ArrayHelper::merge(['value' => $model["_id"]->__toString()], ['class' => 'btn btn-warning _delivered'], ['title' => Yii::t('app', 'Entregado')]));
//else if ($model["status"] == '2')
//    $before = '<span class="btn disabled" title="Rechazado"><i class="fas fa-times "></i></span>';
//else if ($model["status"] == '4')
//    $before = '<span class="btn disabled" title="Entregado" style="background-color: #fdf950; color: #35b431; border: 1px solid; border-bottom-color: #35b431;"><i class="fas fa-rocket"></i></span>';
//else if ($model["status"] == '5') {
//    $before = '<span class="btn disabled" title="Repartiendo" style="background-color: #9c9c9c; color: white"><i class="fas fa-motorcycle"></i></span> ';
//}

$deliveryBoy = DeliveryBoys::findByIdUpdate($model['delivery_boy'] != '' ? $model['delivery_boy'] : -1);

try {
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'layout' => "{items}",
        'options' => [
            'class' => 'table-responsive',
        ],
        'panel' => [
            'heading' => 'CÓDIGO: ' . $model['code'],
            'after' => false,
            'before' => $deliveryBoy ? 'Repartidor' : false,
            'footer' => false,
        ],
        'summary' => false,
        'responsiveWrap' => false,
        'showPageSummary' => true,
        'id' => 'orderModal',
        'pjax' => true,
        'toolbar' => $deliveryBoy ? ($deliveryBoy['last_name'] . ' ' . $deliveryBoy['first_name']) : false,
        'columns' => Items::columnsSite($model),
    ]);
} catch (Exception $e) {
}

