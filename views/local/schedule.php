<?php

use app\models\Locals;
use kartik\widgets\ActiveForm;
use kartik\widgets\TimePicker;
use yii\base\DynamicModel;
use yii\helpers\Html;

/* @var $model Locals */
/* @var $dynamicModel DynamicModel */

$this->title = 'Horarios - ' . $model['name'];

$this->params['breadcrumbs'] = [['label' => $this->title]];

$this->params['home'] = ['local/index', 'id' => $_GET['id']];

$form = ActiveForm::begin(['id' => 'scheduleForm']);
?>
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-12">
                <?php try {
                    echo $form->field($dynamicModel, 'monday_open')->widget(TimePicker::class, [
                        'options' => [
                            'readonly' => true,
                            'class' => 'bg-white',
                        ],
                        'pluginOptions' => [
                            'showSeconds' => false,
                            'showMeridian' => false,
                            'minuteStep' => 1,
                            'defaultTime' => false,
                        ]
                    ]);
                } catch (Exception $e) {
                } ?>
                <?php try {
                    echo $form->field($dynamicModel, 'tuesday_open')->widget(TimePicker::class, [
                        'options' => [
                            'readonly' => true,
                            'class' => 'bg-white',
                        ],
                        'pluginOptions' => [
                            'showSeconds' => false,
                            'showMeridian' => false,
                            'minuteStep' => 1,
                            'defaultTime' => false,
                        ]
                    ]);
                } catch (Exception $e) {
                } ?>
                <?php try {
                    echo $form->field($dynamicModel, 'wednesday_open')->widget(TimePicker::class, [
                        'options' => [
                            'readonly' => true,
                            'class' => 'bg-white',
                        ],
                        'pluginOptions' => [
                            'showSeconds' => false,
                            'showMeridian' => false,
                            'minuteStep' => 1,
                            'defaultTime' => false,
                        ]
                    ]);
                } catch (Exception $e) {
                } ?>
                <?php try {
                    echo $form->field($dynamicModel, 'thursday_open')->widget(TimePicker::class, [
                        'options' => [
                            'readonly' => true,
                            'class' => 'bg-white',
                        ],
                        'pluginOptions' => [
                            'showSeconds' => false,
                            'showMeridian' => false,
                            'minuteStep' => 1,
                            'defaultTime' => false,
                        ]
                    ]);
                } catch (Exception $e) {
                } ?>
                <?php try {
                    echo $form->field($dynamicModel, 'friday_open')->widget(TimePicker::class, [
                        'options' => [
                            'readonly' => true,
                            'class' => 'bg-white',
                        ],
                        'pluginOptions' => [
                            'showSeconds' => false,
                            'showMeridian' => false,
                            'minuteStep' => 1,
                            'defaultTime' => false,
                        ]
                    ]);
                } catch (Exception $e) {
                } ?>
                <?php try {
                    echo $form->field($dynamicModel, 'saturday_open')->widget(TimePicker::class, [
                        'options' => [
                            'readonly' => true,
                            'class' => 'bg-white',
                        ],
                        'pluginOptions' => [
                            'showSeconds' => false,
                            'showMeridian' => false,
                            'minuteStep' => 1,
                            'defaultTime' => false,
                        ]
                    ]);
                } catch (Exception $e) {
                } ?>
                <?php try {
                    echo $form->field($dynamicModel, 'sunday_open')->widget(TimePicker::class, [
                        'options' => [
                            'readonly' => true,
                            'class' => 'bg-white',
                        ],
                        'pluginOptions' => [
                            'showSeconds' => false,
                            'showMeridian' => false,
                            'minuteStep' => 1,
                            'defaultTime' => false,
                        ]
                    ]);
                } catch (Exception $e) {
                } ?>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12">
                <?php try {
                    echo $form->field($dynamicModel, 'monday_close')->widget(TimePicker::class, [
                        'options' => [
                            'readonly' => true,
                            'class' => 'bg-white',
                        ],
                        'pluginOptions' => [
                            'showSeconds' => false,
                            'showMeridian' => false,
                            'minuteStep' => 1,
                            'defaultTime' => false,
                        ]
                    ]);
                } catch (Exception $e) {
                } ?>
                <?php try {
                    echo $form->field($dynamicModel, 'tuesday_close')->widget(TimePicker::class, [
                        'options' => [
                            'readonly' => true,
                            'class' => 'bg-white',
                        ],
                        'pluginOptions' => [
                            'showSeconds' => false,
                            'showMeridian' => false,
                            'minuteStep' => 1,
                            'defaultTime' => false,
                        ]
                    ]);
                } catch (Exception $e) {
                } ?>
                <?php try {
                    echo $form->field($dynamicModel, 'wednesday_close')->widget(TimePicker::class, [
                        'options' => [
                            'readonly' => true,
                            'class' => 'bg-white',
                        ],
                        'pluginOptions' => [
                            'showSeconds' => false,
                            'showMeridian' => false,
                            'minuteStep' => 1,
                            'defaultTime' => false,
                        ]
                    ]);
                } catch (Exception $e) {
                } ?>
                <?php try {
                    echo $form->field($dynamicModel, 'thursday_close')->widget(TimePicker::class, [
                        'options' => [
                            'readonly' => true,
                            'class' => 'bg-white',
                        ],
                        'pluginOptions' => [
                            'showSeconds' => false,
                            'showMeridian' => false,
                            'minuteStep' => 1,
                            'defaultTime' => false,
                        ]
                    ]);
                } catch (Exception $e) {
                } ?>
                <?php try {
                    echo $form->field($dynamicModel, 'friday_close')->widget(TimePicker::class, [
                        'options' => [
                            'readonly' => true,
                            'class' => 'bg-white',
                        ],
                        'pluginOptions' => [
                            'showSeconds' => false,
                            'showMeridian' => false,
                            'minuteStep' => 1,
                            'defaultTime' => false,
                        ]
                    ]);
                } catch (Exception $e) {
                } ?>
                <?php try {
                    echo $form->field($dynamicModel, 'saturday_close')->widget(TimePicker::class, [
                        'options' => [
                            'readonly' => true,
                            'class' => 'bg-white',
                        ],
                        'pluginOptions' => [
                            'showSeconds' => false,
                            'showMeridian' => false,
                            'minuteStep' => 1,
                            'defaultTime' => false,
                        ]
                    ]);
                } catch (Exception $e) {
                } ?>
                <?php try {
                    echo $form->field($dynamicModel, 'sunday_close')->widget(TimePicker::class, [
                        'options' => [
                            'readonly' => true,
                            'class' => 'bg-white',
                        ],
                        'pluginOptions' => [
                            'showSeconds' => false,
                            'showMeridian' => false,
                            'minuteStep' => 1,
                            'defaultTime' => false,
                        ]
                    ]);
                } catch (Exception $e) {
                } ?>
            </div>
        </div>

        <div class="row">
            <div class="col-12 text-right">
                <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']); ?>
            </div>
        </div>
    </div>
<?php ActiveForm::end(); ?>