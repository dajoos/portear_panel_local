<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * User model
 *
 * @property string $message
 * @property string $title
 * @property string $body
 * @property string $city
 */
class Notifications extends ActiveRecord
{
    public $tipo;
    public $titulo;

    public static function tableName(): string
    {
        return 'notifications';
    }

    public function rules(): array
    {
        return [
            [
                [
                    'message',
                ], 'safe'],
            [
                [
                    'tipo',
                    'titulo',
                    'message',
                    'city',
                ], 'required'],
        ];
    }

    public function attributeLabels(): array
    {
        return [
            'id' => 'Id',
            'message' => 'Notificación',
            'tipo' => 'Tipo de notificación',
            'city' => 'Ciudad'
        ];
    }

    public static function notificationExists($title, $body): bool
    {
        if (self::findOne(['title' => $title, 'body' => $body])) {
            return true;
        } else {
            return false;
        }
    }

    public function send($registrationIds, $title, $order, $state, $type, $body): array
    {
        $param = Yii::$app->params['googleapis-fcm'];
        $url = $param['url'];

        $message = [
            'message' => $body,
            'title' => $title,
            'orden' => $order,
            'estado' => $state,
            'orderID' => $order,
            'body' => $body,
        ];

        $notification = [
            'body' => $body,
            'title' => $title,
            'click_action' => 'FLUTTER_NOTIFICATION_CLICK'
        ];

        $authorization = null;

        if ($type == 'android') {
            $authorization = $param['authorization-android'];
        } else if ($type == 'ios') {
            $authorization = $param['authorization-ios'];
        }

        $fields = [
            'registration_ids' => $registrationIds,
            'data' => $message,
            'notification' => $notification,
        ];

        $headers = [
            'Authorization: ' . $authorization,
            'Content-Type: application/json'
        ];

        $json = json_encode($fields);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
        $result = curl_exec($ch);
        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }
        curl_close($ch);
        $response = json_decode($result, true);

        return ['success' => $response['success'], 'failure' => $response['failure']];
    }
}