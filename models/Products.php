<?php

namespace app\models;

use MongoDB\BSON\ObjectId;
use Yii;
use yii\base\Model;

class Products extends Model
{
    public $_id;
    public $name;
    public $description;
    public $created_at;
    public $updated_at;
    public $type;
    public $items;
    public $weighing;
    public $state;
    public $start_date;
    public $end_date;
    public $undefined;
    public $single_date;
    public $day_month;
    public $day_week;
    public $open_time;
    public $close_time;
    public $delete;
    public $deleted_at;
    public $active;

    public function rules(): array
    {
        return [
            [
                [
                    'name',
                    'description',
                    'items',
                    'created_at',
                    'updated_at',
                    'type',
                    'state',
                    'weighing',
                    'open_time',
                    'close_time',
                    'delete',
                    'active',
                ], 'safe'],
            [
                [
                    'name',
                    'description',
                    'updated_at',
                    'type',
                    'state',
                    'weighing',
                    'open_time',
                    'close_time',
                ], 'required'],
            [
                [
                    'weighing'
                ], 'integer'],
            [
                [
                    'start_date',
                    'end_date',
                ],
                'required',
                'when' => function ($model) {
                    return $model->type == 'Rango de fechas';
                }
            ],
            [
                [
                    'undefined',
                ],
                'required',
                'when' => function ($model) {
                    return $model->type == 'Indefinido';
                }
            ],
            [
                [
                    'single_date',
                ],
                'required',
                'when' => function ($model) {
                    return $model->type == 'Fecha única';
                }
            ],
            [
                [
                    'day_month',
                ],
                'required',
                'when' => function ($model) {
                    return $model->type == 'Día del mes';
                }
            ],
            [
                [
                    'day_week',
                ],
                'required',
                'when' => function ($model) {
                    return $model->type == 'Día de semana';
                }
            ],
            [
                [
                    'end_date',
                ], 'compare', 'compareAttribute' => 'start_date', 'operator' => '>'],
            [
                [
                    'close_time',
                ], 'compare', 'compareAttribute' => 'open_time', 'operator' => '>'],
//            [
//                [
//                    'name',
//                ], 'validateUniqueName'],
        ];
    }

    public function validateUniqueName($attribute)
    {
        if (Locals::findByIdName($_GET['id'], $this->name) && !isset($_GET['x'])) {
            $this->addError($attribute, 'Este título ya existe');
        } elseif (isset($_GET['x']) && self::findByIdNameSubId($_GET['id'], $this->name, $_GET['x'])) {
            $this->addError($attribute, 'Ya existe este título');
        }
    }

    public function attributes(): array
    {
        return [
            '_id',
            'name',
            'description',
            'items',
            'created_at',
            'updated_at',
            'deleted_at',
            'type',
            'weighing',
            'state',
            'open_time',
            'close_time',
            'delete',
            'active',
        ];
    }

    public function attributeLabels(): array
    {
        return [
            '_id',
            'name' => 'Título',
            'description' => 'Descripción',
            'items',
            'created_at' => 'Creado',
            'updated_at' => 'Actualizado',
            'type' => 'Tipo de activación',
            'state' => 'Estado',
            'weighing' => 'Ponderación',
            'start_date' => 'Inicio',
            'end_date' => 'Fin',
            'undefined' => 'Indefinido',
            'single_date' => 'Fecha única',
            'day_month' => 'Día del mes',
            'day_week' => 'Día de semana',
            'open_time' => 'Desde',
            'close_time' => 'Hasta',
            'active' => 'Activo',
        ];
    }

    public static function getTypes(): array
    {
        return [
            [
                'type' => 'Indefinido'
            ],
            [
                'type' => 'Fecha única'
            ],
            [
                'type' => 'Rango de fechas'
            ],
            [
                'type' => 'Día del mes'
            ],
            [
                'type' => 'Día de semana'
            ]
        ];
    }

    public static function findByIdNameSubId($id, $name, $oid)
    {
        if (!empty(Yii::$app->mongodb)) {
            $collection = Yii::$app->mongodb->getCollection(Locals::collectionName());
            $result = $collection->aggregate([
                [
                    '$match' => [
                        '_id' => $id
                    ]
                ],
                [
                    '$unwind' => '$products'
                ],
                [
                    '$match' => [
                        'products.name' => $name,
                    ]
                ],
                [
                    '$match' => [
                        'products.delete' => false
                    ]
                ],
                [
                    '$project' => [
                        '_id' => [
                            '$ne' => [
                                '$products._id',
                                new ObjectId($oid)
                            ]
                        ],
                        'name' => '$products.name',
                        'description' => '$products.description',
                        'created_at' => '$products.created_at',
                        'updated_at' => '$products.updated_at',
                        'type' => '$products.type',
                        'weighing' => '$products.weighing',
                        'state' => '$products.state',
                        'open_time' => '$products.open_time',
                        'close_time' => '$products.close_time',
                        'delete' => '$products.delete',
                        'items' => '$products.items',
                        'start_date' => '$products.start_date',
                        'end_date' => '$products.end_date',
                        'undefined' => '$products.undefined',
                        'single_date' => '$products.single_date',
                        'day_month' => '$products.day_month',
                        'day_week' => '$products.day_week',
                        'deleted_at' => '$products.deleted_at',
                    ]
                ]
            ]);
            $validate = new self(array_shift($result));
            return isset($validate['_id']) ? $validate['_id'] : false;
        }
        return [];
    }

    public static function findByIdOid($id, $oid)
    {
        $h = date('H') * 60;
        $m = date('i');
        $now = $h + $m;

        if (!empty(Yii::$app->mongodb)) {
            $collection = Yii::$app->mongodb->getCollection(Locals::collectionName());
            $result = $collection->aggregate([
                [
                    '$match' => [
                        '_id' => $id
                    ]
                ],
                [
                    '$unwind' => '$products'
                ],
                [
                    '$match' => [
                        'products._id' => new ObjectID($oid)
                    ]
                ],
                [
                    '$project' => [
                        '_id' => '$products._id',
                        'name' => '$products.name',
                        'description' => '$products.description',
                        'created_at' => '$products.created_at',
                        'updated_at' => '$products.updated_at',
                        'type' => '$products.type',
                        'weighing' => '$products.weighing',
                        'state' => '$products.state',
                        'open_time' => '$products.open_time',
                        'close_time' => '$products.close_time',
                        'delete' => '$products.delete',
                        'items' => '$products.items',
                        'start_date' => '$products.start_date',
                        'end_date' => '$products.end_date',
                        'undefined' => '$products.undefined',
                        'single_date' => '$products.single_date',
                        'day_month' => '$products.day_month',
                        'day_week' => '$products.day_week',
                        'deleted_at' => '$products.deleted_at',
                        'active' => self::condActive($now),
                    ]
                ],
            ]);
            return new self(array_shift($result));
        }
        return [];
    }

    public static function condActive($now): array
    {
        return [
            '$cond' => [
                ['$eq' => ['$products.state', true]],
                [
                    '$cond' => [
                        [
                            '$gte' => [
                                $now,
                                '$products.open_time'
                            ]
                        ],
                        [
                            '$cond' => [
                                [
                                    '$lte' => [
                                        $now,
                                        '$products.close_time'
                                    ]
                                ],

                                [
                                    '$switch' => [
                                        'branches' => [
                                            [
                                                'case' => [
                                                    '$eq' => ['$products.type', 'Indefinido']
                                                ],
                                                'then' => '$products.undefined'
                                            ],
                                            [
                                                'case' => [
                                                    '$eq' => ['$products.type', 'Fecha única']
                                                ],
                                                'then' => [
                                                    '$cond' => [
                                                        [
                                                            '$eq' => ['$products.single_date', date('Y-m-d')]
                                                        ],
                                                        true,
                                                        false
                                                    ]
                                                ]
                                            ],
                                            [
                                                'case' => [
                                                    '$eq' => ['$products.type', 'Rango de fechas']
                                                ],
                                                'then' => [
                                                    '$cond' => [
                                                        [
                                                            '$gte' => [date('Y-m-d'), '$products.start_date']
                                                        ],
                                                        [
                                                            '$cond' => [
                                                                [
                                                                    '$lte' => [date('Y-m-d'), '$products.end_date']
                                                                ],
                                                                true,
                                                                false
                                                            ]
                                                        ],
                                                        false
                                                    ]
                                                ]
                                            ],
                                            [
                                                'case' => [
                                                    '$eq' => ['$products.type', 'Día del mes']
                                                ],
                                                'then' => [
                                                    '$cond' => [
                                                        [
                                                            '$eq' => ['$products.day_month', date('j')]
                                                        ],
                                                        true,
                                                        false
                                                    ]
                                                ]
                                            ],
                                            [
                                                'case' => [
                                                    '$eq' => ['$products.type', 'Día de semana']
                                                ],
                                                'then' => [
                                                    '$cond' => [
                                                        [
                                                            '$eq' => ['$products.day_week', strtolower(date('l'))]
                                                        ],
                                                        true,
                                                        false
                                                    ]
                                                ]
                                            ]
                                        ],
                                        'default' => false
                                    ]
                                ],
                                false
                            ]
                        ],
                        false
                    ]
                ],
                false
            ]
        ];
    }


    public static function updateProduct($model): int
    {
        if (!empty(Yii::$app->mongodb)) {
            $collection = Yii::$app->mongodb->getCollection(Locals::collectionName());
            return $collection->update(
                [
                    '_id' => $_GET['id'],
                    'products._id' => new ObjectId($_GET['x'])
                ],
                [
                    '$set' => [
                        'products.$.name' => $model['name'],
                        'products.$.description' => $model['description'],
                        'products.$.weighing' => $model['weighing'],
                        'products.$.type' => $model['type'],
                        'products.$.open_time' => $model['open_time'],
                        'products.$.close_time' => $model['close_time'],
                        'products.$.updated_at' => $model['updated_at'],
                        'products.$.state' => $model['state'],
                        'products.$.undefined' => $model['undefined'] ?? false,
                        'products.$.single_date' => $model['single_date'] ?? '',
                        'products.$.start_date' => $model['start_date'] ?? '',
                        'products.$.end_date' => $model['end_date'] ?? '',
                        'products.$.day_month' => $model['day_month'] ?? '',
                        'products.$.day_week' => $model['day_week'] ?? '',
                    ]
                ]
            );
        }
        return 0;
    }

    public static function newProduct($model): int
    {
        if (!empty(Yii::$app->mongodb)) {
            $collection = Yii::$app->mongodb->getCollection(Locals::collectionName());
            return $collection->update(
                [
                    '_id' => $_GET['id'],
                ],
                [
                    '$push' => [
                        'products' => $model,
                    ]
                ]
            );
        }
        return 0;
    }

    public static function deleteProduct(): array
    {
        if (!empty(Yii::$app->mongodb)) {
            $collection = Yii::$app->mongodb->getCollection(Locals::collectionName());
            return $collection->update(
                [
                    '_id' => $_GET['id'],
                    'products._id' => new ObjectId($_GET['x'])
                ],
                [
                    '$set' => [
                        'products.$.deleted_at' => time(),
                        'products.$.delete' => true
                    ]
                ]
            );
        }
        return [];
    }

    public static function stateProduct($state): array
    {
        if (!empty(Yii::$app->mongodb)) {
            $collection = Yii::$app->mongodb->getCollection(Locals::collectionName());
            return $collection->update(
                [
                    '_id' => $_GET['id'],
                    'products._id' => new ObjectId($_GET['x'])
                ],
                [
                    '$set' => [
                        'products.$.updated_at' => time(),
                        'products.$.state' => $state
                    ]
                ]
            );
        }
        return [];
    }

    public static function getValidation($data): ?string
    {
        $value = null;
        switch ($data['type']) {
            case 'Indefinido':
                $value = $data['undefined'] ? 'Activo' : 'Inactivo';
                break;
            case 'Fecha única':
                $value = $data['single_date'];
                break;
            case 'Rango de fechas':
                $value = 'Desde el ' . $data['start_date'] . ' hasta el ' . $data['end_date'];
                break;
            case 'Día del mes':
                $value = 'Los ' . $data['day_month'] . ' de cada mes';
                break;
            case 'Día de semana':
                switch ($data['day_week']) {
                    case 'monday':
                        $value = 'Todos los lunes';
                        break;
                    case 'tuesday':
                        $value = 'Todos los martes';
                        break;
                    case 'wednesday':
                        $value = 'Todos los miércoles';
                        break;
                    case 'thursday':
                        $value = 'Todos los jueves';
                        break;
                    case 'friday':
                        $value = 'Todos los viernes';
                        break;
                    case 'saturday':
                        $value = 'Todos los sábados';
                        break;
                    case 'sunday':
                        $value = 'Todos los domingos';
                        break;
                }
                break;
        }
        return $value;
    }
}