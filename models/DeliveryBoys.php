<?php

namespace app\models;

use Yii;
use yii\mongodb\ActiveRecord;

/**
 * Delivery_boys model
 *
 * @property string $_id
 * @property string $city
 * @property string $first_name
 * @property string $last_name
 * @property string $created_at
 * @property string $updated_at
 * @property string $active
 * @property string $state
 * @property string $orders
 * @property string $location
 * @property string $tokens
 * @property string $vehicles
 */
class DeliveryBoys extends ActiveRecord
{
    public static function collectionName(): string
    {
        return 'delivery_boys';
    }

    public function attributes(): array
    {
        return [
            '_id',
            'first_name',
            'last_name',
            'city',
            'created_at',
            'updated_at',
            'active',
            'state',
            'orders',
            'location',
            'tokens',
            'vehicles'
        ];
    }

    public static function findByIdUpdate($id)
    {
        return self::find()->where(['_id' => $id])->one();
    }

    public static function getDeliveryBoysActive($city): array
    {
        if (!empty(Yii::$app->mongodb)) {
            $collection = Yii::$app->mongodb->getCollection(self::collectionName());
            return $collection->aggregate([
                [
                    '$match' => [
                        'city' => $city,
                        'state' => true,
                        'active' => true,
                    ]
                ],
                [
                    '$addFields' => [
                        'allTokens' => [
                            '$map' => [
                                'input' => '$tokens',
                                'as' => 't',
                                'in' => [
                                    'token' => '$$t.firebase_token',
                                    'so' => '$$t.so',
                                ]
                            ]
                        ],
                    ]
                ],
                [
                    '$unwind' => '$allTokens'
                ],
                [
                    '$project' => [
                        '_id' => 0,
                        'token' => '$allTokens',
                    ]
                ],
                [
                    '$group' => [
                        '_id' => null,
                        'uniqueValues' => [
                            '$addToSet' => '$token',
                        ]
                    ]
                ],
                [
                    '$project' => [
                        '_id' => 0,
                        'firebase_tokens' => '$uniqueValues',
                    ]
                ],
            ]);
        }
        return [];
    }

}