<footer class="main-footer">
    <div class="float-right d-none d-sm-inline">
        Todos los derechos reservados.
    </div>
    <strong>NuovoApps S.A. &copy; <?= date('Y') ?>.</strong>
</footer>