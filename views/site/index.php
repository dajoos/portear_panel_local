<?php

use app\search\OrderSearch;
use hail812\adminlte3\widgets\Callout;
use kartik\grid\GridView;
use yii\bootstrap4\Modal;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

$this->title = 'Inicio';


/* @var $searchModel OrderSearch */
/* @var $dataProvider ActiveDataProvider */

Modal::begin([
    'title' => '<h3>Detalle de la orden</h3>',
    'id' => 'modal',
    'size' => 'modal-lg'
]);
echo "<div class='clearfix' id='modalContent'></div>";
Modal::end();

?>
    <style xmlns="http://www.w3.org/1999/html">
        .table {
            background-color: white;
        }
    </style>
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <?php try {
                    echo Callout::widget([
                        'type' => 'info',
                        'head' => 'Bienvenido a Nubbe - Pide de todo!',
                        'body' => 'Este es el panel de control donde podrá gestionar las órdenes, agregar, actualizar y eliminar información de tus locales.'
                    ]);
                } catch (Exception $e) {
                } ?>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <?php try {
                    echo GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'id' => 'ordersGrid',
                        'pjax' => true,
                        'responsiveWrap' => false,
                        'layout' => "{pager}{summary}{items}{pager}",
                        'pager' => [
                            'firstPageLabel' => 'Inicio',
                            'lastPageLabel' => 'Fin'
                        ],
                        'export' => false,
                        'panel' => [
                            'type' => false,
                            'heading' => false,
                            'after' => false,
                            'before' => HTML::button('0 pedidos nuevos', ArrayHelper::merge(['value' => 'site/update-notification'], ['id' => 'newOrders', 'class' => 'btn btn-info _addNew']))
                        ],
                        'columns' => [
                            [
                                'class' => 'yii\grid\SerialColumn',
                            ],
                            [
                                'attribute' => 'code',
                                'label' => 'Código'
                            ],
                            [
                                'attribute' => 'local',
                                'label' => 'Local',
                                'format' => 'html',
                                'value' => function ($data) {
                                    return '<strong>' . $data['local']['name'] . '</strong>';
                                }
                            ],
                            [
                                'attribute' => 'created_date',
                                'label' => 'Creación',
                                'value' => 'created_date',
                            ],
                            [
                                'attribute' => 'updated_date',
                                'label' => 'Actualización',
                                'value' => 'updated_date',
                            ],
                            [
                                'attribute' => 'subtotal',
                                'filter' => false,
                                'label' => 'Total',
                                'value' => function ($data) {
                                    return '$ ' . number_format($data['subtotal'], 2, ',', '.');
                                }
                            ],
                            [
                                'class' => 'yii\grid\ActionColumn',
                                'header' => 'Acciones',
                                'template' => '{index}',
                                'contentOptions' => ['style' => 'width:100px; white-space:nowrap; text-align:center;'],
                                'buttons' => [
                                    'index' => function ($url, $model) {
                                        if ($model["status"] == '0') {
                                            //$urlReject = Yii::$app->getUrlManager()->createAbsoluteUrl(['site/reject', 'id' => $model["_id"]->__toString()]);
                                            return HTML::button('<i class="fas fa-check"></i>', ArrayHelper::merge(['value' => $model["_id"]->__toString()], ['class' => 'btn btn-success _accept'], ['title' => Yii::t('app', 'Aceptar')])) . ' ' . HTML::button('<i class="fas fa-times"></i>', ArrayHelper::merge(['value' => $model["_id"]->__toString()], ['class' => 'btn btn-danger _reject'], ['title' => Yii::t('app', 'Rechazar')]));
                                        } else if ($model["status"] == '1' || ($model["status"] == '5' && $model['delivery_boy'] == '')) {
                                            return HTML::button('<i class="fas fa-user-plus"></i>', ArrayHelper::merge(['value' => $model["_id"]->__toString()], ['class' => 'btn btn-primary _assign'], ['title' => Yii::t('app', 'Solicitar repartidor')]));// . ' ' . HTML::button('<i class="fas fa-times"></i>', ArrayHelper::merge(['value' => $model["_id"]->__toString()], ['class' => 'btn btn-danger _modalButton'], ['title' => Yii::t('app', 'Rechazar')]));
                                        } else if ($model["status"] == '3')
                                            return '<span class="btn disabled" title="Entregando" style="background-color: #9c9c9c; color: white"><i class="fas fa-spinner "></i></span>';
                                        else if ($model["status"] == '2')
                                            return '<span class="btn disabled" title="Rechazado"><i class="fas fa-times "></i></span>';
                                        else if ($model["status"] == '4')
                                            return '<span class="btn disabled" title="Entregado"><i class="fas fa-check-square"></i></span>';
                                        else if ($model["status"] == '5' && $model['delivery_boy'] != '') {
                                            return '<span class="btn disabled" title="Repartiendo" style="background-color: #9c9c9c; color: white"><i class="fas fa-motorcycle"></i></span> ';//
                                        } else
                                            return null;
                                    }
                                ],
                            ],
                            [
                                'class' => 'yii\grid\ActionColumn',
                                'header' => 'Ver',
                                'contentOptions' => ['style' => 'width:50px; white-space:nowrap;'],
                                'template' => '{detail}',
                                'buttons' => [
                                    'detail' => function ($url, $model) {
                                        return HTML::button('<i class="fas fa-info"></i>', ArrayHelper::merge(['value' => $model["_id"]->__toString()], ['class' => 'btn btn-info _detail'], ['title' => Yii::t('app', 'Detalle')]));
                                    }
                                ],
                            ],
                        ]
                    ]);
                } catch (Exception $e) {
                } ?>
            </div>
        </div>
    </div>
<?php

$urlNewOrders = Yii::$app->getUrlManager()->createAbsoluteUrl('site/get-new-orders');
$urlAccept = Yii::$app->getUrlManager()->createAbsoluteUrl('site/accept');
$urlReject = Yii::$app->getUrlManager()->createAbsoluteUrl('site/reject');
$urlDetail = Yii::$app->getUrlManager()->createAbsoluteUrl('site/detail');
$urlAssign = Yii::$app->getUrlManager()->createAbsoluteUrl('site/assign');
$urlUpdateNotification = Yii::$app->getUrlManager()->createAbsoluteUrl('site/update-notification');
$urlSong = Yii::$app->getUrlManager()->createAbsoluteUrl('song/noty.mp3');
$urlLoading = Yii::$app->getUrlManager()->createAbsoluteUrl('img/cargando.gif');
$script = <<< JS
$(document).ready(function() {
    $('body').on('click','._addNew', function(e){
        $.ajax({
            url: '$urlUpdateNotification',
            type: 'POST',
            dataType: 'json',
            success: function(data){
                if (data.code === 1){
                    $.pjax.reload({container: '#ordersGrid-pjax', timeout: false});
                    //$.pjax.reload({container: '#orderModal-pjax', timeout: false});
                }
            }
        });
        e.preventDefault();
    });
    $('body').on('click','._accept', function(e){
        if (confirm('¿Desea ACEPTAR esta orden?'))
            var val = $(this).val();
            $.ajax({
                url: '$urlAccept',
                type: 'POST',
                data: {id: val},
                dataType: 'json',
                success: function(data){
                    if (data.code === 1){
                        $.pjax.reload({container: '#ordersGrid-pjax', timeout: false});
                        //$.pjax.reload({container: '#orderModal-pjax', timeout: false});
                    }
                }
            });
        e.preventDefault();
    });
    $('body').on('click','._reject', function(e){
        var val = $(this).val();
        $.ajax({
            url: '$urlReject',
            type: 'GET',
            data: {id: val},
            beforeSend: function() {
                $('#modal').modal('show')
                    .find('#modalContent')
                    .html('<div width="100%" style="text-align: center;"><img src="$urlLoading" alt="cargando..."></div>');
            },
            success: function(data){
                $('#modal')
                    .find('#modalContent')
                    .html(data);
            }
        });
        e.preventDefault();
    });
    $('body').on('click','._detail', function(e){
        var val = $(this).val();
        $.ajax({
            url: '$urlDetail',
            type: 'GET',
            data: {id: val},
            beforeSend: function() {
                $('#modal').modal('show')
                    .find('#modalContent')
                    .html('<div width="100%" style="text-align: center;"><img src="$urlLoading" alt="cargando..."></div>');
            },
            success: function(data){
                $('#modal')
                    .find('#modalContent')
                    .html(data);
            }
        });
        e.preventDefault();
    });
    $('body').on('click','._assign', function(e){
        if (confirm('¿Desea SOLICITAR UN REPARTIDOR para esta orden?'))
            var val = $(this).val();
            $.ajax({
                url: '$urlAssign',
                type: 'POST',
                data: {id: val},
                dataType: 'json',
                success: function(data){
                    if (data.code === 1){
                        $.pjax.reload({container: '#ordersGrid-pjax', timeout: false});
                        //$.pjax.reload({container: '#orderModal-pjax', timeout: false});
                    }
                }
            });
        e.preventDefault();
    });
    // $('body').on('click','._stop_android', function(e){
    //     if (confirm('¿Desea DETENER los pedidos de TODOS los clientes?'))
    //         var val = $(this).val();
    //         $.ajax({
    //             url: val,
    //             type: 'POST',
    //             dataType: 'json',
    //             success: function(data){
    //                 console.log(data);
    //                 if (data.code === 1){
    //                     $.pjax.reload({container: '#ordersGrid-pjax', timeout: false});
    //                 }
    //             }
    //         });
    //     e.preventDefault();
    // });
    // $('body').on('click','._start_android', function(e){
    //     if (confirm('¿Desea INICIAR los pedidos de TODOS los clientes?'))
    //         var val = $(this).val();
    //         $.ajax({
    //             url: val,
    //             type: 'POST',
    //             dataType: 'json',
    //             success: function(data){
    //                 console.log(data);
    //                 if (data.code === 1){
    //                     $.pjax.reload({container: '#ordersGrid-pjax', timeout: false});
    //                 }
    //             }
    //         });
    //     e.preventDefault();
    // });
    setInterval(function() {
        $.ajax({
            url: '$urlNewOrders',
            type: 'POST',
            dataType: 'json',
            success: function(data){
                if (data.code > 0){
                    let snd = new Audio('$urlSong');
                    snd.play();
                    $('#newOrders').text(data.code + ' órdenes nuevas');
                    document.getElementById('newOrders').style.backgroundColor = '#5cb85c';
                    document.getElementById('newOrders').style.borderColor = '#5cb85c';
                    console.log(data.code);
                }
            }
        });
    },1000);
});
JS;
$this->registerJs($script);
?>