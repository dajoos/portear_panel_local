<?php

namespace app\models;

use yii\base\Model;

class Schedule extends Model
{
    public $monday_open;
    public $monday_close;
    public $tuesday_open;
    public $tuesday_close;
    public $wednesday_open;
    public $wednesday_close;
    public $thursday_open;
    public $thursday_close;
    public $friday_open;
    public $friday_close;
    public $saturday_open;
    public $saturday_close;
    public $sunday_open;
    public $sunday_close;

    public function rules(): array
    {
        return [
            [
                [
                    'monday_open',
                    'monday_close',
                    'tuesday_open',
                    'tuesday_close',
                    'wednesday_open',
                    'wednesday_close',
                    'thursday_open',
                    'thursday_close',
                    'friday_open',
                    'friday_close',
                    'saturday_open',
                    'saturday_close',
                    'sunday_open',
                    'sunday_close'
                ], 'safe'],
            [
                [
                    'monday_close',
                    'tuesday_close',
                    'wednesday_close',
                    'thursday_close',
                    'friday_close',
                    'saturday_close',
                    'sunday_close'
                ], 'validateTime'],
        ];
    }

    public function validateTime($attribute)
    {
        if ($attribute == 'monday_close' && strlen($this->monday_open) > 0 && strlen($this->monday_close) > 0) {
            if (((date('H', strtotime($this->monday_open)) * 60) + (date('i', strtotime($this->monday_open)))) >= ((date('H', strtotime($this->monday_close)) * 60) + (date('i', strtotime($this->monday_close))))) {
                $this->addError($attribute, 'Debe ser mayor la hora de cerrar');
            }
        }
        if ($attribute == 'tuesday_close' && strlen($this->tuesday_open) > 0 && strlen($this->tuesday_close) > 0) {
            if (((date('H', strtotime($this->tuesday_open)) * 60) + (date('i', strtotime($this->tuesday_open)))) >= ((date('H', strtotime($this->tuesday_close)) * 60) + (date('i', strtotime($this->tuesday_close))))) {
                $this->addError($attribute, 'Debe ser mayor la hora de cerrar');
            }
        }
        if ($attribute == 'wednesday_close' && strlen($this->wednesday_open) > 0 && strlen($this->wednesday_close) > 0) {
            if (((date('H', strtotime($this->wednesday_open)) * 60) + (date('i', strtotime($this->wednesday_open)))) >= ((date('H', strtotime($this->wednesday_close)) * 60) + (date('i', strtotime($this->wednesday_close))))) {
                $this->addError($attribute, 'Debe ser mayor la hora de cerrar');
            }
        }
        if ($attribute == 'thursday_close' && strlen($this->thursday_open) > 0 && strlen($this->thursday_close) > 0) {
            if (((date('H', strtotime($this->thursday_open)) * 60) + (date('i', strtotime($this->thursday_open)))) >= ((date('H', strtotime($this->thursday_close)) * 60) + (date('i', strtotime($this->thursday_close))))) {
                $this->addError($attribute, 'Debe ser mayor la hora de cerrar');
            }
        }
        if ($attribute == 'friday_close' && strlen($this->friday_open) > 0 && strlen($this->friday_close) > 0) {
            if (((date('H', strtotime($this->friday_open)) * 60) + (date('i', strtotime($this->friday_open)))) >= ((date('H', strtotime($this->friday_close)) * 60) + (date('i', strtotime($this->friday_close))))) {
                $this->addError($attribute, 'Debe ser mayor la hora de cerrar');
            }
        }
        if ($attribute == 'saturday_close' && strlen($this->saturday_open) > 0 && strlen($this->saturday_close) > 0) {
            if (((date('H', strtotime($this->saturday_open)) * 60) + (date('i', strtotime($this->saturday_open)))) >= ((date('H', strtotime($this->saturday_close)) * 60) + (date('i', strtotime($this->saturday_close))))) {
                $this->addError($attribute, 'Debe ser mayor la hora de cerrar');
            }
        }
        if ($attribute == 'sunday_close' && strlen($this->sunday_open) > 0 && strlen($this->sunday_close) > 0) {
            if (((date('H', strtotime($this->sunday_open)) * 60) + (date('i', strtotime($this->sunday_open)))) >= ((date('H', strtotime($this->sunday_close)) * 60) + (date('i', strtotime($this->sunday_close))))) {
                $this->addError($attribute, 'Debe ser mayor la hora de cerrar');
            }
        }
    }

    public function attributeLabels(): array
    {
        return [
            'monday_open' => 'Lunes desde',
            'monday_close' => 'Lunes hasta',
            'tuesday_open' => 'Mates desde',
            'tuesday_close' => 'Martes hasta',
            'wednesday_open' => 'Miércoles desde',
            'wednesday_close' => 'Miércoles hasta',
            'thursday_open' => 'Jueves desde',
            'thursday_close' => 'Jueves hasta',
            'friday_open' => 'Viernes desde',
            'friday_close' => 'Viernes hasta',
            'saturday_open' => 'Sábado desde',
            'saturday_close' => 'Sábado hasta',
            'sunday_open' => 'Domingo desde',
            'sunday_close' => 'Domingo hasta',
        ];
    }

    public function attributes(): array
    {
        return [
            'monday_open',
            'monday_close',
            'tuesday_open',
            'tuesday_close',
            'wednesday_open',
            'wednesday_close',
            'thursday_open',
            'thursday_close',
            'friday_open',
            'friday_close',
            'saturday_open',
            'saturday_close',
            'sunday_open',
            'sunday_close'
        ];
    }

}