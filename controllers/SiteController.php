<?php

namespace app\controllers;

use app\models\PasswordResetRequestForm;
use app\models\ResetPasswordForm;
use app\models\User;
use yii\base\Exception;
use yii\base\InvalidArgumentException;
use app\models\DeliveryBoys;
use app\models\Notifications;
use app\models\Orders;
use app\search\OrderSearch;
use Yii;
use yii\base\DynamicModel;
use yii\data\ArrayDataProvider;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors(): array
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => [
                    'logout',
                    'index',
                    'accept',
                    'reject',
                    'assign',
                    'detail',
                    'get-new-orders',
                    'update-notification',
                    'profile',
                ],
                'rules' => [
                    [
                        'actions' => [
                            'index',
                            'get-new-orders',
                            'update-notification',
                            'profile',
                        ],
                        'allow' => true,
                        'roles' => ['@']
                    ],
                    [
                        'actions' => [
                            'accept',
                            'assign',
                        ],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function () {
                            return Orders::isOwnerOrder(); //validar con post id
                        },
                    ],
                    [
                        'actions' => [
                            'reject',
                        ],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function () {
                            return Orders::isOwnerOrder(); //validar con post dynamic id
                        },
                    ],
                    [
                        'actions' => [
                            'detail',
                        ],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function () {
                            return Orders::isOwnerOrder(); //validar con get id
                        },
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions(): array
    {
        if (Yii::$app->user->isGuest)
            $this->layout = 'main-info';
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * @return string
     */
    public function actionIndex(): string
    {
        $searchModel = new OrderSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', ['dataProvider' => $dataProvider, 'searchModel' => $searchModel]);
    }

    /**
     * @return Response|string
     */
    public function actionLogin()
    {
        $this->layout = 'main-info';
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * @return Response
     */
    public function actionLogout(): Response
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * @return string
     */
    public function actionContact(): string
    {
        if (Yii::$app->user->isGuest)
            $this->layout = 'main-info';
        return $this->render('contact');
    }

    /**
     * @return string
     */
    public function actionAbout(): string
    {
        if (Yii::$app->user->isGuest)
            $this->layout = 'main-info';
        return $this->render('about');
    }

    /**
     * @return string
     */
    public function actionTnc(): string
    {
        if (Yii::$app->user->isGuest)
            $this->layout = 'main-info';
        return $this->render('tnc');
    }

    public function actionGetNewOrders()
    {
        $new = Orders::getNewOrders();
        return json_encode(['code' => count($new)]);
    }

    public function actionUpdateNotification()
    {
        Orders::updateNotification();
        return json_encode(['code' => 1]);
    }

    public function actionAccept()
    {
        $send = [];
        if (isset($_POST['id'])) {
            $id = $_POST['id'];
            $orderDetail = Orders::findByIdUpdate($id);
            Orders::updateStatus($id, 1, null, null);
            if (isset($orderDetail['firebase_token'])) {
                $noty = new Notifications();
                $body = Yii::$app->params['messages']['preparing'];
                $send = $noty->send([$orderDetail['firebase_token']], 'ORDEN: ' . $orderDetail['code'], $orderDetail['code'], 1, $orderDetail['so'], $body);
            }
        }
        return json_encode(['code' => 1, 'notification' => $send]);
    }

    public function actionReject()
    {
        $model = new DynamicModel([
            'id' => $_GET['id'],
            'reject_reason'
        ]);
        $model->addRule('reject_reason', 'required', ['message' => 'Motivo del rechazo es requerido']);
        $model->addRule('id', 'required');
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $send = [];
            if (isset($_POST['DynamicModel']['id'])) {
                $id = $_POST['DynamicModel']['id'];
                $reject_reason = $_POST['DynamicModel']['reject_reason'];
                $orderDetail = Orders::findByIdUpdate($id);
                $cash_back = ($orderDetail['payment']['method'] == 'Tarjeta' && $orderDetail['payment']['paid_out']) ? 'Su reembolso se hará entre 5 y 10 días hábiles' : '';
                Orders::updateStatus($id, 2, $reject_reason, $cash_back);
                if (isset($orderDetail['firebase_token'])) {
                    $noty = new Notifications();
                    $body = Yii::$app->params['messages']['reject'];
                    $send = $noty->send([$orderDetail['firebase_token']], 'ORDEN: ' . $orderDetail['code'], $orderDetail['code'], 2, $orderDetail['so'], $body);
                }
            }
            return json_encode(['code' => 1, 'notification' => $send]);
        }

        $modelX = Orders::findByIdUpdate($_GET['id']);
        $dataProvider = new ArrayDataProvider([
            'allModels' => $modelX['items'],
            'pagination' => [
                'pageSize' => 20
            ],
            'sort' => false,
        ]);
        return $this->renderAjax('_reject', ['model' => $model, 'modelX' => $modelX, 'dataProvider' => $dataProvider]);
    }

    public function actionAssign()
    {
        $send = [];
        $send_boy_android = [];
        $send_boy_ios = [];
        if (isset($_POST['id'])) {
            $id = $_POST['id'];
            $orderDetail = Orders::findByIdUpdate($id);
//            $localDetail = Locals::findByIdUpdate($orderDetail['local_id']);
            if (isset($orderDetail['firebase_token'])) {
                $delivery_boy = DeliveryBoys::getDeliveryBoysActive($orderDetail['city']);
                if ($delivery_boy) {
                    $tokens_delivery_boy_android = [];
                    $tokens_delivery_boy_ios = [];
                    foreach ($delivery_boy as $val) {
                        foreach ($val['firebase_tokens'] as $value) {
                            if ($value['so'] == 'android')
                                $tokens_delivery_boy_android[] = $value['token'];
                            if ($value['so'] == 'ios')
                                $tokens_delivery_boy_ios[] = $value['token'];
                        }
                    }
                    $noty = new Notifications();
//                    $body = Yii::$app->params['messages']['assignOrder'];
                    $body_boy = Yii::$app->params['messages']['deliveryBoy'];
//                    $send = $noty->send([$orderDetail['firebase_token']], 'ORDEN: ' . $orderDetail['code'], $orderDetail['code'], 2, $orderDetail['so'], $body);
                    if (count($tokens_delivery_boy_android) > 0)
                        $send_boy_android = $noty->send($tokens_delivery_boy_android, 'ORDEN: ' . $orderDetail['code'], $orderDetail['code'], 2, $orderDetail['so'], $body_boy);
                    if (count($tokens_delivery_boy_ios) > 0)
                        $send_boy_ios = $noty->send($tokens_delivery_boy_ios, 'ORDEN: ' . $orderDetail['code'], $orderDetail['code'], 2, $orderDetail['so'], $body_boy);
                    Orders::updateDeliveryBoyAssign($id, 5);
//                    DeliveryBoys::updateAll(['orders' => (int)((int)$delivery_boy['orders'] + 1)], ['_id' => $delivery_boy['_id']]);
                    return json_encode(['code' => 1, 'notification' => [$send, $send_boy_android, $send_boy_ios]]);
                }
            }
        }
        return json_encode(['code' => 1, 'notification' => [$send, $send_boy_android, $send_boy_ios]]);
    }

    /**
     * @return string
     */
    public function actionDetail(): string
    {
        $model = Orders::findByIdUpdate($_GET['id']);
        $dataProvider = new ArrayDataProvider([
            'allModels' => $model['items'],
            'pagination' => [
                'pageSize' => 20
            ],
            'sort' => false,
        ]);
        return $this->renderAjax('_detail', ['model' => $model, 'dataProvider' => $dataProvider]);
    }

    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if (Yii::$app->user->isGuest)
            $this->layout = 'main-info';
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Revise su correo electrónico para obtener más instrucciones.');
                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('danger', 'Lo sentimos, no podemos restablecer la contraseña de la dirección de correo electrónico proporcionada.');
            }
        }
        return $this->render('request-password-reset-token', ['model' => $model,]);
    }

    public function actionResetPassword($token)
    {
        if (Yii::$app->user->isGuest)
            $this->layout = 'main-info';
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidArgumentException $e) {
            Yii::$app->session->setFlash('danger', $e->getMessage());
            return $this->goHome();
        }

        try {
            if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
                Yii::$app->session->setFlash('success', 'Nueva contraseña guardada.');
                return $this->goHome();
            }
        } catch (Exception $e) {
        }

        return $this->render('reset-password', ['model' => $model]);
    }

    public function actionProfile()
    {
        $model = null;
        if (isset(Yii::$app->user->identity->id)) {
            $model = User::findByUsername(Yii::$app->user->identity->username);
            $model->scenario = 'update';
            if ($model->load(Yii::$app->request->post())) {
                if ($model->validate()) {
                    if ($model->validatePassword($_POST['User']['password_aux'])) {
                        if ($model->save()) {
                            Yii::$app->session->setFlash('info', 'Información actualizada con éxito.');
                            return $this->redirect(['profile']);
                        }
                    } else {
                        $model->addError('password_aux', 'Contraseña incorrecta');
                        $model->password = null;
                    }
                }
            }
        }
        return $this->render('profile', ['model' => $model]);
    }
}
