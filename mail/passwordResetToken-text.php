<?php

/* @var $this yii\web\View */
/* @var $user app\models\User */

$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['site/reset-password', 'token' => $user->password_reset_token]);
?>
    Usuario: <?= $user->username ?>,
    Siga el enlace a continuación para restablecer su contraseña:
<?= $resetLink ?>