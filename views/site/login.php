<?php

use app\models\LoginForm;
use kartik\widgets\ActiveForm;
use yii\base\InvalidConfigException;
use yii\helpers\Html;

/* @var $model LoginForm */

?>
<div class="login-box">
    <div class="card">
        <div class="card-body login-card-body">
            <p class="login-box-msg">Iniciar sesión</p>

            <?php $form = ActiveForm::begin(['id' => 'login-form']) ?>

            <?php try {
                echo $form->field($model, 'username', [
                    'options' => ['class' => 'form-group has-feedback'],
                ])
                    ->label(false)
                    ->textInput(['placeholder' => $model->getAttributeLabel('username')]);
            } catch (InvalidConfigException $e) {
            } ?>

            <?php try {
                echo $form->field($model, 'password', [
                    'options' => ['class' => 'form-group has-feedback'],
                ])
                    ->label(false)
                    ->passwordInput(['placeholder' => $model->getAttributeLabel('password')]);
            } catch (InvalidConfigException $e) {
            } ?>

            <div class="row">
                <div class="col-12">
                    <?= Html::submitButton('Entrar', ['class' => 'btn btn-primary btn-block']) ?>
                </div>
            </div>

            <?php ActiveForm::end(); ?>
            <?= Html::a('Recuperar contraseña', 'request-password-reset') ?>
        </div>
        <!-- /.login-card-body -->
    </div>

</div>