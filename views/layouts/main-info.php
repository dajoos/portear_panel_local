<?php

/* @var $this View */

use hail812\adminlte3\assets\AdminLteAsset;
use hail812\adminlte3\assets\PluginAsset;
use yii\base\InvalidConfigException;
use yii\web\View;

$assetDir = '';

/* @var $content string */

AdminLteAsset::register($this);
try {
    $this->registerCssFile('https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700');
} catch (InvalidConfigException $e) {
}
try {
    $this->registerCssFile('https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css');
} catch (InvalidConfigException $e) {
}
PluginAsset::register($this)->add(['fontawesome', 'icheck-bootstrap']);

?>
<?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>">
    <head>
        <link rel="shortcut icon" href="<?= Yii::getAlias('@web') . '/img/favicon.ico' ?>" type="image/x-icon"/>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Nubbe | Iniciar sesión</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?php $this->registerCsrfMetaTags() ?>
        <?php $this->head() ?>
    </head>
    <body class="hold-transition login-page">
    <?php $this->beginBody() ?>
    <div class="login-logo">
        <img src="<?= Yii::getAlias('@web') . '/img/logo.png' ?>" width="300px" alt="Logo">
    </div>
    <?= $this->render('principal', ['content' => $content]) ?>
    <?php $this->endBody() ?>
    </body>
    </html>
<?php $this->endPage() ?>