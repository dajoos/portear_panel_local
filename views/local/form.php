<?php

use app\models\Locals;
use kalyabin\maplocation\SelectMapLocationAssets;
use kalyabin\maplocation\SelectMapLocationWidget;
use kartik\widgets\ActiveForm;
use kartik\widgets\FileInput;
use yii\base\InvalidConfigException;
use yii\helpers\Html;
use yii\web\JsExpression;

/* @var $model Locals */

SelectMapLocationAssets::$googleMapApiKey = Yii::$app->params['google-map-api-key'];
SelectMapLocationAssets::register($this);

$this->title = 'Actualizar - ' . $model['name'];

$this->params['breadcrumbs'] = [['label' => $this->title]];

$this->params['home'] = ['local/index', 'id' => $_GET['id']];

?>
<div class="container-fluid">
    <?php $form = ActiveForm::begin(['id' => 'registerForm', 'options' => ['enctype' => 'multipart/form-data']]); ?>
    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-12">
            <?php try {
                echo $form->field($model, 'photo')->widget(FileInput::class, [
                    'options' => [
                        'accept' => 'image/*',
                        'multiple' => false,
                    ],
                    'pluginOptions' => [
                        'showPreview' => true,
                        'showCaption' => true,
                        'showUpload' => false,
                        'showRemove' => false,
                        'initialPreview' => [
                            $model['photo'] ? Html::img(Yii::getAlias('@web') . '/img/local_profile/' . $model['photo'], ['width' => '100%']) : null
                        ],
                        'overwriteInitial' => false
                    ]
                ]);
            } catch (Exception $e) {
            } ?>
            <?php try {
                echo $form->field($model, 'name')->textInput(['readonly' => true]);
            } catch (InvalidConfigException $e) {
            } ?>
            <?php try {
                echo $form->field($model, 'city')->textInput(['readonly' => true]);
            } catch (InvalidConfigException $e) {
            } ?>
            <?= $form->field($model, 'description'); ?>
            <?= $form->field($model, 'phone'); ?>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-12">
            <?= $form->field($model, 'email'); ?>
            <?= $form->field($model, 'website'); ?>
            <?php try {
                echo $form->field($model, 'situation')->widget(SelectMapLocationWidget::class, [
                    'attributeLatitude' => 'location[coordinates][0]',
                    'attributeLongitude' => 'location[coordinates][1]',
                    'draggable' => true,
                    'googleMapApiKey' => Yii::$app->params['google-map-api-key'],
                    'jsOptions' => [
                        'onLoadMap' => new JsExpression("
                        function(map)
                        {
                            document.getElementById('locals-situation').style.display = 'none';
                            var mapOptions = {
                               center: new google.maps.LatLng( " . $model['location']['coordinates'][0] . ", " . $model['location']['coordinates'][1] . "),
                               zoom: 17,
                               panControl: true,
                               zoomControl: true,
                               mapTypeId: google.maps.MapTypeId.ROADMAP,
                           };
                           map.setOptions(mapOptions);
                        }"
                        )],
                ]);
            } catch (Exception $e) {
            } ?>
            <div class="row">
                <div class="col-12 text-right">
                    <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']); ?>
                </div>
            </div>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>