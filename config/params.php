<?php

return [
    'user' => [
        'passwordResetTokenExpire' => 3600,
        'passwordMinLength' => 10
    ],
    'supportEmail' => 'soporte.nubbeapp@gmail.com',
    'bsVersion' => '4.x',
    'hail812/yii2-adminlte3' => [
        'pluginMap' => [
            'sweetalert2' => [
                'css' => 'sweetalert2-theme-bootstrap-4/bootstrap-4.min.css',
                'js' => 'sweetalert2/sweetalert2.min.js'
            ]
        ]
    ],
    'google-map-api-key' => 'AIzaSyAgyEhA7rFe_08pG556tTI2AvKKlLC1d9w',
    'googleapis-fcm' => [
        'url' => 'https://fcm.googleapis.com/fcm/send',
        'authorization-android' => 'key=AAAAZNJUy7E:APA91bE0hn3OOUeukOMeVFJgt2wQe4Rv3AKZuBLf_zWxkGt-O94E-aToXGk__c0D0sBBdIRjrtIri53_uABQdxsrC7tA92tV38mHRucSIlkdKfFuJoUI8Y997ava-tpsE3DY8LANZntJ',
        'authorization-ios' => 'key=AAAAZNJUy7E:APA91bE0hn3OOUeukOMeVFJgt2wQe4Rv3AKZuBLf_zWxkGt-O94E-aToXGk__c0D0sBBdIRjrtIri53_uABQdxsrC7tA92tV38mHRucSIlkdKfFuJoUI8Y997ava-tpsE3DY8LANZntJ'
    ],
    'messages' => [
        'preparing' => 'Su orden ha sido aceptada!',
        'delivered' => 'Su orden ha sido recogida!',
        'reject' => 'Su orden ha sido rechazada',
        'complete' => 'Su orden ha sido entregada',
        'pickOrder' => 'Su orden pronto será entregada',
        'assignOrder' => 'Su orden ha sido asignada a un repartidor',
        'deliveryBoy' => 'Se solicita repartidor para este pedido',
    ],
];
