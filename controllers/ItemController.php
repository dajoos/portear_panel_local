<?php

namespace app\controllers;

use app\models\Items;
use app\models\Products;
use app\models\Locals;
use app\models\User;
use app\search\ItemSearch;
use Gumlet\ImageResize;
use MongoDB\BSON\ObjectId;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\UploadedFile;

class ItemController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors(): array
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => [
                    'index',
                    'new',
                    'detail',
                    'update',
                ],
                'rules' => [
                    [
                        'actions' => [
                            'index',
                            'new',
                            'detail',
                            'update',
                        ],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function () {
                            return User::isOwner();
                        },
                    ],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions(): array
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    private static function newAndUpdate($model, $type, $commission): array
    {
        $validation = false;
        $model->updated_at = time();
        $model->delete = false;
        if ($model->validate()) {
            $commission = (float)$commission;
            $model->price = (float)number_format($_POST['Items']['price'], 2, '.', '');
            $model->price_final = (float)number_format(($_POST['Items']['price'] * (float)($commission == 0 ? '1' : ($commission + 1))), 2, '.', '');
            $model->_id = new ObjectID();
            $model->weighing = (int)$model['weighing'];
            $model->state = ($_POST['Items']['state'] == 1);
            switch ($type) {
                case 'new':
                    $model->created_at = time();
                    if (Items::insertItem($model)) {
                        $validation = true;
                    }
                    break;
                case 'update':
                    $update = Items::updateItem($model);
                    if ($update) {
                        $validation = true;
                    } elseif ($update == 0) {
                        $validation = true;
                    }
                    break;
            }
        }
        $model->state = $model['state'] ? 1 : 0;
        return ['model' => $model, 'validation' => $validation];
    }

    public function actionDelete()
    {
        Items::deleteItem();
        return json_encode(['code' => 1]);
    }

    public function actionDisable()
    {
        Items::stateItem(false);
        return json_encode(['code' => 1]);
    }

    public function actionEnable()
    {
        Items::stateItem(true);
        return json_encode(['code' => 1]);
    }

    /**
     * @return string
     */
    public function actionIndex(): string
    {
        $model = Locals::findByIdUpdate($_GET['id']);
        $product = Products::findByIdOid($_GET['id'], $_GET['x']);
        $searchModel = new ItemSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', ['searchModel' => $searchModel, 'dataProvider' => $dataProvider, 'model' => $model, 'product' => $product]);
    }

    /**
     * @throws \Gumlet\ImageResizeException
     */
    public function actionNew()
    {
        $local = Locals::findByIdUpdate($_GET['id']);
        $product = Products::findByIdOid($_GET['id'], $_GET['x']);
        $model = new Items();
        $model->scenario = 'create';
        if ($model->load(Yii::$app->request->post())) {
            $coverPage = UploadedFile::getInstance($model, 'image');
            $validateImage = true;
            if ($coverPage != null) {
                $img = 'item_' . $_GET['id'] . '_' . $_GET['x'] . '_' . time();
                $path = '../web/img/products/' . $img . '.' . $coverPage->extension;
                $model->image = $img . '.' . $coverPage->extension;
            }
            if ($validateImage) {
                $result = $this->newAndUpdate($model, 'new', $local->commission);
                if ($result['validation']) {
                    if (isset($path)) {
                        $coverPage->saveAs($path);
                        $image = new ImageResize($path);
                        $image->crop(500, 500, true, ImageResize::CROPCENTER);
                        $image->save($path);
                    }
                    Yii::$app->session->setFlash('success', $_POST['Items']['name'] . ' ingresado exitosamente ');
                    return $this->redirect(['index', 'id' => $_GET['id'], 'x' => $_GET['x']]);
                } else {
                    $model = $result['model'];
                    $model->image = $_POST['Items']['image'];
                }
            }
        }
        return $this->render('form', ['model' => $model, 'local' => $local, 'product' => $product]);
    }

    /**
     * @return string
     */
    public function actionDetail(): string
    {
        $model = Items::findByIdOid($_GET['id'], $_GET['x'], $_GET['oid']);
        return $this->renderAjax('detail', ['model' => $model]);
    }

    /**
     * @throws \Gumlet\ImageResizeException
     */
    public function actionUpdate()
    {
        $local = Locals::findByIdUpdate($_GET['id']);
        $product = Products::findByIdOid($_GET['id'], $_GET['x']);
        $model = Items::findByIdXOid($_GET['id'], $_GET['x'], $_GET['oid']);
        $image_back = $model['image'];
        $name = $model['name'];
        $model->state = $model['state'] ? 1 : 0;
        if ($model->load(Yii::$app->request->post())) {
            $coverPage = UploadedFile::getInstance($model, 'image');
            $validateImage = true;
            if ($coverPage != null) {
                $img = 'item_' . $_GET['id'] . '_' . $_GET['x'] . '_' . time();
                $path = '../web/img/products/' . $img . '.' . $coverPage->extension;
                $model->image = $img . '.' . $coverPage->extension;
            } else {
                $model->image = $image_back;
            }
            if ($validateImage) {
                $result = $this->newAndUpdate($model, 'update', $local->commission);
                if ($result['validation']) {
                    if (isset($path)) {
                        $coverPage->saveAs($path);
                        $image = new ImageResize($path);
                        $image->crop(500, 500, true, ImageResize::CROPCENTER);
                        $image->save($path);
                    }
                    Yii::$app->session->setFlash('info', $_POST['Items']['name'] . ' actualizado exitosamente ');
                    return $this->redirect(['index', 'id' => $_GET['id'], 'x' => $_GET['x']]);
                } else {
                    $model = $result['model'];
                    $model->image = $image_back;
                }
            }
        }
        return $this->render('form', ['model' => $model, 'local' => $local, 'name' => $name, 'product' => $product]);
    }
}
