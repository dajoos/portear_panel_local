<?php

namespace app\search;

use app\models\Orders;
use app\models\User;
use MongoDB\BSON\Regex;
use Yii;
use yii\base\Model;
use yii\data\ArrayDataProvider;

class OrderSearch extends Orders
{
    public function rules(): array
    {
        return [
            [
                [
                    '_id',
                    'code',
                    'lat',
                    'lon',
                    'address',
                    'reference',
                    'total',
                    'local',
                    'created_date',
                    'updated_date',
                    'commission',
                    'subtotal',
                    'surcharge',
                    'version',
                    'distance',
                    'local_id',
                    'firebase_token',
                    'phone',
                    'city',
                    'user_id',
                    'created_at',
                    'updated_at',
                    'payment',
                    'personalized',
                    'items',
                    'status',
                ], 'safe'],
        ];
    }

    public function scenarios(): array
    {
        return Model::scenarios();
    }

    public function search($params)
    {
        $code = $params["OrderSearch"]["code"] ?? '';
        $local = $params["OrderSearch"]["local"] ?? '';
        $created = $params["OrderSearch"]["created_date"] ?? '';
        $updated = $params["OrderSearch"]["updated_date"] ?? '';

        if (!User::getLocalsOwner()) {
            return [];
        }
        if (count(User::getLocalsOwner()['locals']) == 0) {
            return [];
        }
        $locals = [];
        foreach (User::getLocalsOwner()['locals'] as $array) {
            $locals[] = $array['local'];
        }

        if (!empty(Yii::$app->mongodb)) {
            $collection = Yii::$app->mongodb->getCollection(Orders::collectionName());
            $result = $collection->aggregate([
                [
                    '$match' => [
                        'local_id' => [
                            '$in' => $locals
                        ],
                        'status' => [
                            '$ne' => -1
                        ],
                        '$or' => [
                            [
                                'payment.method' => 'Efectivo'
                            ],
                            [
                                'payment.paid_out' => true
                            ]
                        ]
                    ]
                ],
                [
                    '$addFields' => [
                        'codeSearch' => ['$toString' => '$code'],
                        'local_fk' => ['$toObjectId' => '$local_id'],
                        'created_date' => [
                            '$dateToString' => [
                                'format' => '%G-%m-%d %H:%M:%S',
                                'date' => [
                                    '$toDate' => [
                                        '$multiply' => [['$subtract' => ['$created_at', 18000]], 1000]
                                    ]
                                ]
                            ]
                        ],
                        'updated_date' => [
                            '$dateToString' => [
                                'format' => '%G-%m-%d %H:%M:%S',
                                'date' => [
                                    '$toDate' => [
                                        '$multiply' => [['$subtract' => ['$updated_at', 18000]], 1000]
                                    ]
                                ]
                            ]
                        ]
                    ]
                ],
                [
                    '$lookup' => [
                        'from' => 'locals',
                        'localField' => 'local_fk',
                        'foreignField' => '_id',
                        'as' => 'local'
                    ]
                ],
                [
                    '$unwind' => '$local'
                ],
                [
                    '$match' => [
                        'codeSearch' => new Regex(".*$code.*", 'i')
                    ]
                ],
                [
                    '$match' => [
                        'local.name' => new Regex(".*$local.*", 'i')
                    ]
                ],
                [
                    '$match' => [
                        'created_date' => new Regex(".*$created.*", 'i')
                    ]
                ],
                [
                    '$match' => [
                        'updated_date' => new Regex(".*$updated.*", 'i')
                    ]
                ],
                [
                    '$sort' => [
                        'code' => -1
                    ]
                ],
            ]);

            $this->load($params);

            return new ArrayDataProvider([
                'allModels' => $result,
                'pagination' => [
                    'pageSize' => 20
                ],
                'sort' => false,
            ]);
        }
        return [];

    }
}
