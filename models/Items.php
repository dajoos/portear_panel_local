<?php

namespace app\models;

use MongoDB\BSON\ObjectId;
use Yii;
use yii\base\Model;

class Items extends Model
{
    public $_id;
    public $name;
    public $description;
    public $price;
    public $price_final;
    public $image;
    public $created_at;
    public $updated_at;
    public $deleted_at;
    public $state;
    public $delete;
    public $weighing;

    public function rules(): array
    {
        return [
            [
                [
                    '_id',
                    'name',
                    'description',
                    'price',
                    'image',
                    'created_at',
                    'updated_at',
                    'deleted_at',
                    'state',
                    'delete',
                    'weighing',
                ], 'safe'],
            [
                [
                    'name',
                    'description',
                    'price',
                    'updated_at',
                    'state',
                    'weighing',
                ], 'required'],
            ['image', 'required', 'on' => 'create'],
            ['image', 'file', 'on' => 'create', 'skipOnEmpty' => true],
            ['image', 'image', 'extensions' => 'png, jpg, jpeg', /*'minWidth' => 500, 'maxWidth' => 500, 'minHeight' => 500, 'maxHeight' => 500*/],
            [
                [
                    'price',
                ], 'compare', 'compareValue' => '0', 'operator' => '>='],
//            [
//                [
//                    'name',
//                ], 'validateUniqueName'],
        ];
    }


    public function validateUniqueName($attribute)
    {
        if (Locals::findByIdOidName($_GET['id'], $_GET['x'], $this->name) && !isset($_GET['oid'])) {
            $this->addError($attribute, 'Este item ya existe');
        } elseif (isset($_GET['oid']) && self::findByIdNameSubIdOid($_GET['id'], $this->name, $_GET['x'], $_GET['oid'])) {
            $this->addError($attribute, 'Ya existe este item');
        }
    }

    public function attributes(): array
    {
        return [
            '_id',
            'name',
            'description',
            'price',
            'price_final',
            'image',
            'created_at',
            'updated_at',
            'deleted_at',
            'state',
            'delete',
            'weighing'
        ];
    }

    public function attributeLabels(): array
    {
        return [
            '_id',
            'name' => 'Nombre',
            'description' => 'Descripción',
            'price' => 'Precio',
            'price_final' => 'Precio final',
            'image' => 'Imagen',
            'created_at' => 'Creado',
            'updated_at' => 'Actualizado',
            'deleted_at',
            'state' => 'Estado',
            'delete',
            'weighing' => 'Ponderación',
        ];
    }

    public static function findByIdNameSubIdOid($id, $name, $x, $oid)
    {
        if (!empty(Yii::$app->mongodb)) {
            $collection = Yii::$app->mongodb->getCollection(Locals::collectionName());
            $result = $collection->aggregate([
                [
                    '$match' => [
                        '_id' => $id
                    ]
                ],
                [
                    '$unwind' => '$products'
                ],
                [
                    '$match' => [
                        'products._id' => new ObjectId($x)
                    ]
                ],
                [
                    '$unwind' => '$products.items'
                ],
                [
                    '$match' => [
                        'products.items.name' => $name,
                        'products.items.delete' => false
                    ],
                ],
                [
                    '$project' => [
                        '_id' => [
                            '$ne' => [
                                '$products.items._id',
                                new ObjectId($oid)
                            ]
                        ],
                        'name' => '$products.items.name',
                        'description' => '$products.items.description',
                        'price' => '$products.items.price',
                        'price_final' => '$products.items.price_final',
                        'weighing' => '$products.items.weighing',
                        'image' => '$products.items.image',
                        'created_at' => '$products.items.created_at',
                        'updated_at' => '$products.items.updated_at',
                        'deleted_at' => '$products.items.deleted_at',
                        'state' => '$products.items.state',
                        'delete' => '$products.items.delete',
                    ]
                ]
            ]);
            $validate = new self(array_shift($result));
            return isset($validate['_id']) ? $validate['_id'] : false;
        }
        return [];
    }

    public static function findByIdXOid($id, $x, $oid)
    {
        if (!empty(Yii::$app->mongodb)) {
            $collection = Yii::$app->mongodb->getCollection(Locals::collectionName());
            $result = $collection->aggregate([
                [
                    '$match' => [
                        '_id' => $id
                    ]
                ],
                [
                    '$unwind' => '$products'
                ],
                [
                    '$match' => [
                        'products._id' => new ObjectId($x),
                    ]
                ],
                [
                    '$unwind' => '$products.items'
                ],
                [
                    '$match' => [
                        'products.items._id' => new ObjectId($oid)
                    ]
                ],
                [
                    '$match' => [
                        'products.items.delete' => false
                    ]
                ],
                [
                    '$project' => [
                        '_id' => '$products.items._id',
                        'name' => '$products.items.name',
                        'description' => '$products.items.description',
                        'price' => '$products.items.price',
                        'price_final' => '$products.items.price_final',
                        'weighing' => '$products.items.weighing',
                        'image' => '$products.items.image',
                        'created_at' => '$products.items.created_at',
                        'updated_at' => '$products.items.updated_at',
                        'deleted_at' => '$products.items.deleted_at',
                        'state' => '$products.items.state',
                        'delete' => '$products.items.delete'
                    ]
                ]
            ]);
            return new self(array_shift($result));
        }
        return [];
    }

    public static function insertItem($model): int
    {
        if (!empty(Yii::$app->mongodb)) {
            $collection = Yii::$app->mongodb->getCollection(Locals::collectionName());
            return $collection->update(
                [
                    '_id' => $_GET['id'],
                    'products._id' => new ObjectId($_GET['x'])
                ],
                [
                    '$push' => [
                        'products.$.items' => $model,
                    ]
                ]
            );
        }
        return 0;
    }

    public static function deleteItem(): array
    {
        if (!empty(Yii::$app->mongodb)) {
            $collection = Yii::$app->mongodb->getCollection(Locals::collectionName());
            return $collection->update(
                [
                    '_id' => $_GET['id'],
                    'products._id' => new ObjectId($_GET['oid'])
                ],
                [
                    '$set' => [
                        'products.$[].items.$[i].deleted_at' => time(),
                        'products.$[].items.$[i].delete' => true
                    ]
                ],
                [
                    'arrayFilters' => [
                        [
                            'i._id' => new ObjectId($_GET['x'])
                        ],
                    ]
                ]
            );
        }
        return [];
    }

    public static function stateItem($state): array
    {
        if (!empty(Yii::$app->mongodb)) {
            $collection = Yii::$app->mongodb->getCollection(Locals::collectionName());
            return $collection->update(
                [
                    '_id' => $_GET['id'],
                    'products._id' => new ObjectId($_GET['oid'])
                ],
                [
                    '$set' => [
                        'products.$[].items.$[i].updated_at' => time(),
                        'products.$[].items.$[i].state' => $state
                    ]
                ],
                [
                    'arrayFilters' => [
                        [
                            'i._id' => new ObjectId($_GET['x'])
                        ],
                    ]
                ]
            );
        }
        return [];
    }

    public static function updateItem($model): int
    {
        if (!empty(Yii::$app->mongodb)) {
            $collection = Yii::$app->mongodb->getCollection(Locals::collectionName());
            return $collection->update(
                [
                    '_id' => $_GET['id'],
                    'products._id' => new ObjectId($_GET['x'])
                ],
                [
                    '$set' => [
                        'products.$[].items.$[i].name' => $model['name'],
                        'products.$[].items.$[i].description' => $model['description'],
                        'products.$[].items.$[i].price' => $model['price'],
                        'products.$[].items.$[i].price_final' => $model['price_final'],
                        'products.$[].items.$[i].image' => $model['image'],
                        'products.$[].items.$[i].updated_at' => $model['updated_at'],
                        'products.$[].items.$[i].state' => $model['state'],
                        'products.$[].items.$[i].weighing' => $model['weighing']
                    ]
                ],
                [
                    'arrayFilters' => [
                        [
                            'i._id' => new ObjectId($_GET['oid'])
                        ],
                    ]
                ]
            );
        }
        return 0;
    }

    public static function findByIdOid($id, $x, $oid)
    {
        if (!empty(Yii::$app->mongodb)) {
            $collection = Yii::$app->mongodb->getCollection(Locals::collectionName());
            $result = $collection->aggregate([
                [
                    '$match' => [
                        '_id' => $id
                    ]
                ],
                [
                    '$unwind' => '$products'
                ],
                [
                    '$match' => [
                        'products._id' => new ObjectId($x),
                    ]
                ],
                [
                    '$unwind' => '$products.items'
                ],
                [
                    '$match' => [
                        'products.items._id' => new ObjectId($oid),
                    ]
                ],
                [
                    '$match' => [
                        'products.items.delete' => false
                    ]
                ],
                [
                    '$project' => [
                        '_id' => '$products.items._id',
                        'name' => '$products.items.name',
                        'description' => '$products.items.description',
                        'price' => '$products.items.price',
                        'price_final' => '$products.items.price_final',
                        'weighing' => '$products.items.weighing',
                        'image' => '$products.items.image',
                        'created_at' => '$products.items.created_at',
                        'updated_at' => '$products.items.updated_at',
                        'deleted_at' => '$products.items.deleted_at',
                        'state' => '$products.items.state',
                        'delete' => '$products.items.delete',
                    ]
                ]
            ]);
            return new self(array_shift($result));
        }
        return [];
    }

    public static function columnsSite($model): array
    {
        return [
            [
                'attribute' => 'itemProduct',
                'group' => true,
                'groupedRow' => true,
                'format' => 'raw',
                'label' => 'Categoría',
                'value' => function ($data) {
                    return '<strong>' . $data['itemProduct'] . '</strong>';
                }
            ],
            [
                'attribute' => 'itemName',
                'label' => 'Nombre',
            ],
            [
                'attribute' => 'itemDescription',
                'label' => 'Descripción',
            ],
            [
                'attribute' => 'itemQuantity',
                'label' => 'Cantidad',
            ],
            [
                'attribute' => 'itemPrice',
                'label' => 'Precio',
                'value' => function ($data) {
                    return '$' . number_format($data['itemPrice'], '2', '.', '');
                }
            ],
            [
                'attribute' => 'itemSubtotal',
                'label' => 'Total',
                'pageSummary' => function () use ($model) {
                    return '$' . number_format($model['subtotal'], '2', '.', '');
                },
                'value' => function ($data) {
                    return '$' . number_format($data['itemSubtotal'], '2', '.', '');
                }
            ],
        ];
    }
}