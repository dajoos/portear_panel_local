<?php

/* @var $this yii\web\View */

$this->title = 'Términos y condiciones';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="container-fluid">
    <p align="justify">
    <h3>1. Ámbito de aplicación / celebración del contrato</h3></p>
    <p align="justify">1.1. Nuestros siguientes Términos y Condiciones de Entrega y Pago serán aplicables a todas las
        transacciones comerciales.</p>
    <p align="justify">1.2. Nuestros Términos y Condiciones de Entrega y Pago se aplicarán exclusivamente. No
        aceptaremos términos y condiciones del cliente que entren en conflicto o se desvíen de nuestros Términos y
        condiciones de entrega y pago, a menos que hayamos dado nuestro consentimiento expreso por escrito para su
        aplicación.</p>
    <p align="justify">1.3. Los acuerdos colaterales, las enmiendas a estos términos y condiciones, así como las
        desviaciones de estos términos y condiciones, deben acordarse por escrito.</p>
    <p align="justify">
    <h3>2. Precio de compra</h3></p>
    <p align="justify">2.1. Sujeto a un acuerdo expreso, nuestros precios se cotizan en DOLARES, con valores previamente
        establecidos por los locales asociado a sus productos, más los impuestos establecidos por la empresa NUBBE por
        servicios tecnológicos prestados y valores por flete.</p>
    <p align="justify">2.2. Si en general reducimos o aumentamos nuestro precio durante la vigencia del contrato de
        compra, los precios modificados se aplicarán a las cantidades que aún no se hayan comprado.</p>
    <p align="justify">2.3. En el caso de que no se haya pactado expresamente un precio dentro de la app (PIDE DE TODO),
        se aplicarán precios acordados con el cliente.</p>
    <p align="justify">
    <h3>3. Información relacionada con la aplicación</h3></p>
    <p align="justify">3.1. En la medida en que proporcionemos información, lo haremos sobre la base de nuestro mejor
        conocimiento, nuestra experiencia y datos entregados por el restaurante. La información proporcionada será
        meramente informativa y de apoyo. No proporcionaremos ninguna garantía sobre la exactitud de dicha información
        puesto que es responsabilidad del local respaldarla en sus productos.</p>
    <p align="justify">3.2. La información obtenida por parte del cliente como nombre, email y número de contacto será
        utilizada únicamente para garantizar la ejecución del proceso de principio a fin y no será divulgada a terceros
        bajo ningún concepto.</p>
    <p align="justify">
    <h3>4. Entrega</h3></p>
    <p align="justify">4.1. El cliente recibirá el pedido en el plazo de entrega acordado dentro de la aplicación salvo
        en casos de gran demanda por parte del local, en cuyo caso será notificado, y tendrá la opción cancelar su
        pedido. Si el cliente no acepta la recepción del pedido, sin ningún motivo aceptable para el rechazo no seremos
        responsables de los valores generados por la compra y transporte, y nos reservamos el derecho de bloquear al
        cliente y no aceptar futuras transacciones.</p>
    <p align="justify">4.2. Si se ha acordado un plazo de entrega fijo, el cliente puede, en caso de incumplimiento en
        la entrega por el cual nosotros tengamos la culpa declarar su inconformidad por escrito a través de nuestros
        contactos de correo o llamar directamente a gerencia para llegar a un acuerdo que le satisfaga.</p>
    <p align="justify">4.3. Si los plazos de entrega o ejecución no se cumplen por motivos ajenos a nuestro control,
        como fuerza mayor, guerra, ataques terroristas, restricciones de movilidad, disputas laborales, o si lo anterior
        afecta a nuestros proveedores de productos, los plazos acordados se extenderán por un período razonable, incluso
        si en ese momento estamos en incumplimiento.</p>
    <p align="justify">
    <h3>5. Privacidad, protección de datos y sociedad de la información</h3></p>
    <p align="justify">El usuario tutor, antes de descargar la app, debe haber aceptado las condiciones de privacidad y
        acceso a utilidades descritas por la “Store” donde se realizó la descarga. El servidor donde se aloja nuestro
        gestor de contenidos cumple con todas las medidas de seguridad técnicas para garantizar la seguridad y
        confidencialidad de los datos. Así mismo, tanto nuestro gestor como las comunicaciones con éste respetan en todo
        momento la normativa vigente en materia de protección de datos de carácter personal.</p>
</div>
