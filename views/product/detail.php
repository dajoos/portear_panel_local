<?php

use app\models\Products;
use kartik\detail\DetailView;

/* @var $model Products */

?>
<div class="container-fluid">
    <?php try {
        echo DetailView::widget([
            'model' => $model,
            'options' => ['class' => 'bg-white'],
            'attributes' => [
                'name',
                'description',
                [
                    'attribute' => 'created_at',
                    'format' => 'html',
                    'value' => function () use ($model) {
                        return date('Y-m-d H:i:s', $model['created_at']);
                    }
                ],
                [
                    'attribute' => 'updated_at',
                    'format' => 'html',
                    'value' => function () use ($model) {
                        return date('Y-m-d H:i:s', $model['updated_at']);
                    }
                ],
                'type',
                [
                    'attribute' => 'type',
                    'label' => 'Validación',
                    'value' => function () use ($model) {
                        return Products::getValidation($model);
                    },
                ],
                'weighing',
                [
                    'attribute' => 'open_time',
                    'format' => 'html',
                    'value' => function () use ($model) {
                        return gmdate('H:i', $model['open_time'] * 60);
                    }
                ],
                [
                    'attribute' => 'close_time',
                    'format' => 'html',
                    'value' => function () use ($model) {
                        return gmdate('H:i', $model['close_time'] * 60);
                    }
                ],
                [
                    'attribute' => 'active',
                    'format' => 'html',
                    'value' => function () use ($model) {
                        return $model['active'] ? '<strong style="color:green">Si</strong>' : '<strong style="color:tomato">No</strong>';
                    }
                ],
                [
                    'attribute' => 'state',
                    'format' => 'html',
                    'value' => function () use ($model) {
                        return $model['state'] ? '<strong style="color:green">Activado</strong>' : '<strong style="color:tomato">Desactivado</strong>';
                    }
                ],
            ]
        ]);
    } catch (Exception $e) {
    }
    ?>
</div>