<?php

use app\models\Products;
use app\search\ProductSearch;
use kartik\grid\GridView;
use kartik\widgets\ActiveForm;
use yii\base\InvalidConfigException;
use yii\bootstrap4\Modal;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $model Products */
/* @var $searchModel ProductSearch */
/* @var $dataProvider ActiveDataProvider */

$this->title = 'Productos - ' . $model['name'];

$this->params['breadcrumbs'] = [['label' => 'Agregar nuevo producto', 'url' => ['product/new', 'id' => $_GET['id']]], ['label' => $this->title]];

$this->params['home'] = ['local/index', 'id' => $_GET['id']];

Modal::begin([
    'title' => '<h3>Detalle del producto</h3>',
    'id' => 'modal',
]);
echo "<div class='clearfix' id='modalContent'></div>";
Modal::end();

Pjax::begin([
    'id' => 'productsGrid',
    'timeout' => false,
    'enablePushState' => false,
]);
?>
    <style>
        .table {
            background-color: white;
        }
    </style>
    <div class="container-fluid">
        <?php
        $form = ActiveForm::begin([
            'id' => 'menu-product-search-form',
            'action' => ['product/index', 'id' => $_GET['id']],
            'method' => 'get',
            'options' => ['data-pjax' => true]
        ]);
        ?>
        <div class="row pl-2">
            <div class="w-75">
                <?php try {
                    echo $form->field($searchModel, 'name')->textInput()->label(false);
                } catch (InvalidConfigException $e) {
                } ?>
            </div>
            <div class="pl-1 pb-3">
                <?= Html::submitButton('<span class="fas fa-search"></span>', ['class' => 'btn btn-success']); ?>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
        <div class="row">
            <div class="col-12">
                <?php
                if ($dataProvider->totalCount > 0) {
                    try {
                        echo GridView::widget([
                            'dataProvider' => $dataProvider,
                            'layout' => "{pager}{summary}{items}{pager}",
                            'options' => [
                                'class' => 'table-responsive',
                            ],
                            'responsiveWrap' => false,
                            'pager' => [
                                'firstPageLabel' => 'Inicio',
                                'lastPageLabel' => 'Fin'
                            ],
                            'columns' => [
                                [
                                    'class' => 'yii\grid\SerialColumn',
                                ],
                                [
                                    'attribute' => 'name',
                                    'label' => 'Título',
                                ],
                                [
                                    'attribute' => 'type',
                                    'label' => 'Tipo',
                                ],
                                [
                                    'attribute' => 'type',
                                    'label' => 'Validación',
                                    'value' => function ($data) {
                                        return Products::getValidation($data);
                                    },
                                ],
                                [
                                    'attribute' => 'type',
                                    'label' => 'Horario',
                                    'value' => function ($data) {
                                        return gmdate('H:i', $data['open_time'] * 60) . ' - ' . gmdate('H:i', $data['close_time'] * 60);
                                    },
                                ],
                                [
                                    'attribute' => 'weighing',
                                    'label' => 'Ponderación',
                                ],
                                [
                                    'attribute' => 'active',
                                    'label' => 'Activo',
                                    'format' => 'html',
                                    'value' => function ($data) {
                                        return $data['active'] ? '<strong style="color:green">Si</strong>' : '<strong style="color:tomato">No</strong>';
                                    }
                                ],
                                [
                                    'attribute' => 'state',
                                    'label' => 'Estado',
                                    'format' => 'html',
                                    'value' => function ($data) {
                                        return $data['state'] ? '<strong style="color:green">Activado</strong>' : '<strong style="color:tomato">Desactivado</strong>';
                                    }
                                ],
                                [
                                    'class' => 'yii\grid\ActionColumn',
                                    'template' => '{items} {view} {update} {state} {delete}',
                                    'contentOptions' => ['style' => 'width:250px; white-space:nowrap;'],
                                    'header' => 'Acciones',
                                    'buttons' => [
                                        'items' => function ($url, $model) {
                                            return Html::a('<i class="fas fa-list-ol"></i>', ['item/index', 'id' => $_GET['id'], 'x' => $model['_id']->__toString()], ['class' => 'btn btn-warning', 'title' => Yii::t('app', 'Ver items'), 'data-pjax' => 0]);
                                        },
                                        'view' => function ($url, $model) {
                                            return HTML::button('<i class="far fa-eye"></i>', ArrayHelper::merge(['value' => 'detail?id=' . $_GET['id'] . '&x=' . $model['_id']->__toString()], ['class' => 'btn btn-info _modalButton'], ['title' => Yii::t('app', 'Detalle del producto')]));
                                        },
                                        'update' => function ($url, $model) {
                                            return Html::a('<i class="far fa-edit"></i>', ['update', 'id' => $_GET['id'], 'x' => $model['_id']->__toString()], ['class' => 'btn btn-primary', 'title' => Yii::t('app', 'Editar producto'), 'data-pjax' => 0]);
                                        },
                                        'state' => function ($url, $model) {
                                            if ($model['state'])
                                                return HTML::button('<i class="far fa-times-circle"></i>', ArrayHelper::merge(['value' => $model['_id']->__toString()], ['class' => 'btn btn-danger _disableState'], ['title' => Yii::t('app', 'Desactivar producto')]));
                                            return HTML::button('<i class="far fa-check-circle"></i>', ArrayHelper::merge(['value' => $model['_id']->__toString()], ['class' => 'btn btn-success _enableState'], ['title' => Yii::t('app', 'Activar producto')]));
                                        },
                                        'delete' => function ($url, $model) {
                                            return HTML::button('<i class="far fa-trash-alt"></i>', ArrayHelper::merge(['value' => $model['_id']->__toString()], ['class' => 'btn btn-secondary _delete'], ['title' => Yii::t('app', 'Eliminar producto')]));
                                        },
                                    ],
                                ],
                            ]
                        ]);
                    } catch (Exception $e) {
                    }
                } else {
                    ?>
                    <div class="empty"
                         style="padding: .75rem; border: 1px solid #dee2e6; background-color: rgba(0,0,0,.05);">No se
                        encontraron resultados.
                    </div>
                    <?php
                }
                ?>
            </div>
        </div>
    </div>
<?php
Pjax::end();

$urlDelete = Yii::$app->getUrlManager()->createAbsoluteUrl('product/delete');
$urlDisable = Yii::$app->getUrlManager()->createAbsoluteUrl('product/disable');
$urlEnable = Yii::$app->getUrlManager()->createAbsoluteUrl('product/enable');

$val = $_GET['id'];

$script = <<< JS
$(document).ready(function() {
    $('body').on('click','._delete', function(e){
        if (confirm('¿Desea ELIMINAR este producto?'))
            var valX = $(this).val();
            var valId = '$val';
            $.ajax({
                url: '$urlDelete',
                type: 'GET',
                data: {id: valId, x: valX},
                dataType: 'json',
                success: function(){
                    $.pjax.reload({container: '#productsGrid', timeout: false});            
                }
            });
        e.preventDefault();
    });
    $('body').on('click','._disableState', function(e){
        if (confirm('¿Desea DESACTIVAR este producto?'))
            var valX = $(this).val();
            var valId = '$val';
            $.ajax({
                url: '$urlDisable',
                type: 'GET',
                data: {id: valId, x: valX},
                dataType: 'json',
                success: function(){
                    $.pjax.reload({container: '#productsGrid', timeout: false});
                }
            });
        e.preventDefault();
    });
    $('body').on('click','._enableState', function(e){
        if (confirm('¿Desea ACTIVAR este producto?'))
            var valX = $(this).val();
            var valId = '$val';
            $.ajax({
                url: '$urlEnable',
                type: 'GET',
                data: {id: valId, x: valX},
                dataType: 'json',
                success: function(){
                    $.pjax.reload({container: '#productsGrid', timeout: false});
                }
            });
        e.preventDefault();
    });
});
JS;
$this->registerJs($script);