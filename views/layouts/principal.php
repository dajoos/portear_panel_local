<?php

/* @var $content string */

use hail812\adminlte3\widgets\Alert;

if (Yii::$app->session->hasFlash('success')) {
    try {
        echo Alert::widget([
            'type' => 'success',
            'title' => 'Muy bien',
            'body' => Yii::$app->session->getFlash('success'),
        ]);
    } catch (Exception $e) {
    }
} else if (Yii::$app->session->hasFlash('danger')) {
    try {
        echo Alert::widget([
            'type' => 'danger',
            'title' => 'Hubo un error',
            'body' => Yii::$app->session->getFlash('danger'),
        ]);
    } catch (Exception $e) {
    }
} else if (Yii::$app->session->hasFlash('info')) {
    try {
        echo Alert::widget([
            'type' => 'info',
            'title' => 'Infórmate',
            'body' => Yii::$app->session->getFlash('info'),
        ]);
    } catch (Exception $e) {
    }
} else if (Yii::$app->session->hasFlash('warning')) {
    try {
        echo Alert::widget([
            'type' => 'warning',
            'title' => 'Alerta',
            'body' => Yii::$app->session->getFlash('warning'),
        ]);
    } catch (Exception $e) {
    }
}
echo $content;