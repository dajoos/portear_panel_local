<?php

use app\models\Items;
use kartik\detail\DetailView;
use yii\helpers\Html;

/* @var $model Items */

?>
<div class="container-fluid">
    <?= Html::img(Yii::getAlias('@web') . '/img/products/' . $model['image'], ['width' => '100%']) ?>
    <?php try {
        echo DetailView::widget([
            'model' => $model,
            'options' => ['class' => 'bg-white'],
            'attributes' => [
                'name',
                'description',
                [
                    'attribute' => 'created_at',
                    'format' => 'html',
                    'value' => function () use ($model) {
                        return date('Y-m-d H:i:s', $model['created_at']);
                    }
                ],
                [
                    'attribute' => 'price',
                    'format' => 'html',
                    'value' => function () use ($model) {
                        return '$ ' . $model['price'];
                    }
                ],
                [
                    'attribute' => 'price_final',
                    'format' => 'html',
                    'value' => function () use ($model) {
                        return '<strong>$ ' . $model['price_final'] . '</strong>';
                    }
                ],
                [
                    'attribute' => 'updated_at',
                    'format' => 'html',
                    'value' => function () use ($model) {
                        return date('Y-m-d H:i:s', $model['updated_at']);
                    }
                ],
                'weighing',
                [
                    'attribute' => 'state',
                    'format' => 'html',
                    'value' => function () use ($model) {
                        return $model['state'] ? '<strong style="color:green">Activado</strong>' : '<strong style="color:tomato">Desactivado</strong>';
                    }
                ],
            ]
        ]);
    } catch (Exception $e) {
    }
    ?>
</div>