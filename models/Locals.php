<?php

namespace app\models;

use MongoDB\BSON\ObjectId;
use Yii;
use yii\mongodb\ActiveRecord;

/**
 * Locals model
 *
 * @property string $_id
 * @property string $name
 * @property string $created_at
 * @property string $updated_at
 * @property string $photo
 * @property string $phone
 * @property string $email
 * @property string $website
 * @property string $city
 * @property string $situation
 * @property string $description
 * @property string $type
 * @property string $products
 * @property string $schedule
 * @property string $location
 * @property boolean $state
 * @property string $active
 * @property string $commission
 */
class Locals extends ActiveRecord
{
    public static function collectionName(): string
    {
        return 'locals';
    }

    public function rules(): array
    {
        return [
            [
                [
                    '_id',
                    'name',
                    'created_at',
                    'photo',
                    'phone',
                    'email',
                    'website',
                    'city',
                    'situation',
                    'description',
                    'type',
                    'products',
                    'schedule',
                    'location',
                    'state',
                    'active'
                ], 'safe'],
            ['photo', 'required', 'on' => 'create'],
            [['photo'], 'file', 'on' => 'create', 'skipOnEmpty' => true],
            [['photo'], 'image', 'extensions' => 'png, jpg, jpeg', /*'minWidth' => 800, 'maxWidth' => 800, 'minHeight' => 500, 'maxHeight' => 500*/],
            [
                [
                    'name',
                    'situation',
                    'description',
                    'email',
                    'type',
                    'city',
                    'phone',
                ], 'required'],
            [['website'], 'url', 'defaultScheme' => 'http'],
            [['email'], 'email', 'message' => 'Email no es válido.'],
            [['phone', 'commission'], 'number'],
        ];
    }

    public function attributes(): array
    {
        return [
            '_id',
            'name',
            'created_at',
            'updated_at',
            'photo',
            'phone',
            'email',
            'website',
            'city',
            'situation',
            'description',
            'type',
            'state',
            'active',
            'location',
            'schedule',
            'products',
            'commission',
        ];
    }

    public function attributeLabels(): array
    {
        return [
            '_id',
            'name' => 'Nombre',
            'created_at' => 'Creado',
            'photo' => 'Portada',
            'phone' => 'Teléfono',
            'email' => 'Correo',
            'website' => 'Sitio web',
            'city' => 'Ciudad',
            'situation' => 'Ubicación',
            'description' => 'Descripción',
            'type' => 'Tipo',
            'products' => 'Productos',
            'schedule' => 'Schedule',
            'location' => 'Locación',
            'state' => 'Estado',
            'active' => 'Activo',
            'commission' => 'Comisión'
        ];
    }

    public static function findByIdUpdate($id)
    {
        return self::find()->where(['_id' => $id])->one();
    }

    public static function findByIdName($id, $name)
    {
        return self::find()->where(['_id' => $id])->andWhere(['products.name' => $name])->andWhere(['products.delete' => false])->one();
    }

    public static function findByIdOidName($id, $oid, $name)
    {
        if (!empty(Yii::$app->mongodb)) {
            $collection = Yii::$app->mongodb->getCollection(Locals::collectionName());
            $result = $collection->aggregate([
                [
                    '$match' => [
                        '_id' => $id
                    ]
                ],
                [
                    '$unwind' => '$products'
                ],
                [
                    '$match' => [
                        'products._id' => new ObjectId($oid)
                    ]
                ],
                [
                    '$unwind' => '$products.items'
                ],
                [
                    '$match' => [
                        'products.items.name' => $name,
                        'products.items.delete' => false
                    ],
                ],
                [
                    '$project' => [
                        'name' => [
                            '$eq' => ['$products.items.name', $name]
                        ],
                        'description' => '$products.items.description',
                        'price' => '$products.items.price',
                        'price_final' => '$products.items.price_final',
                        'weighing' => '$products.items.weighing',
                        'image' => '$products.items.image',
                        'created_at' => '$products.items.created_at',
                        'updated_at' => '$products.items.updated_at',
                        'deleted_at' => '$products.items.deleted_at',
                        'state' => '$products.items.state',
                        'delete' => '$products.items.delete',
                    ]
                ]
            ]);
            $validate = new Items(array_shift($result));
            return isset($validate['name']) ? $validate['name'] : false;
        }
        return [];
    }

    public static function findById($id)
    {
        $h = date('H') * 60;
        $m = date('i');
        $now = $h + $m;
        if (!empty(Yii::$app->mongodb)) {
            $collection = Yii::$app->mongodb->getCollection(self::collectionName());
            $result = $collection->aggregate([
                [
                    '$match' => [
                        '_id' => $id
                    ]
                ],
                [
                    '$project' => [
                        'name' => 1,
                        'created_at' => 1,
                        'photo' => 1,
                        'phone' => 1,
                        'email' => 1,
                        'website' => 1,
                        'city' => 1,
                        'situation' => 1,
                        'schedule' => 1,
                        'description' => 1,
                        'type' => 1,
                        'location' => 1,
                        'state' => [
                            '$cond' => [
                                [
                                    '$eq' => [
                                        '$active',
                                        true
                                    ]
                                ],
                                [
                                    '$cond' => [
                                        [
                                            '$eq' => [
                                                '$state',
                                                true
                                            ]
                                        ],
                                        [
                                            '$cond' => [
                                                [
                                                    '$gte' => [
                                                        $now,
                                                        '$schedule.' . strtolower(date('l')) . '.start'
                                                    ]
                                                ],
                                                [
                                                    '$cond' => [
                                                        [
                                                            '$lte' => [
                                                                $now,
                                                                '$schedule.' . strtolower(date('l')) . '.end'
                                                            ]
                                                        ],
                                                        true,
                                                        false
                                                    ]
                                                ],
                                                false
                                            ]
                                        ],
                                        false
                                    ]
                                ],
                                false
                            ]
                        ],
                        'active' => 1
                    ]
                ],
                [
                    '$limit' => 1
                ],
            ]);
            return new self(array_shift($result));
        }
        return [];
    }

    public static function findByOwner()
    {
        if (!isset(Yii::$app->user->identity->locals))
            return [];
        $ids = [];
        foreach (Yii::$app->user->identity->locals as $value) {
            array_push($ids, $value['local']);
        }
        return self::find()->where(['in', '_id', $ids])->asArray()->all();
    }

}