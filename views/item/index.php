<?php

use app\models\Locals;
use app\models\Products;
use app\search\ItemSearch;
use kartik\grid\GridView;
use kartik\widgets\ActiveForm;
use yii\base\InvalidConfigException;
use yii\bootstrap4\Modal;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $model Locals */
/* @var $product Products */
/* @var $searchModel ItemSearch */
/* @var $dataProvider ActiveDataProvider */

$this->title = 'Items - ' . $product['name'] . ' - ' . $model['name'];

$this->params['breadcrumbs'] = [['label' => 'Productos', 'url' => ['product/index', 'id' => $_GET['id']]], ['label' => 'Agregar nuevo item', 'url' => ['item/new', 'id' => $_GET['id'], 'x' => $_GET['x']]], ['label' => $this->title]];

$this->params['home'] = ['local/index', 'id' => $_GET['id']];

Modal::begin([
    'title' => '<h3>Detalle del item</h3>',
    'id' => 'modal',
]);
echo "<div class='clearfix' id='modalContent'></div>";
Modal::end();

Pjax::begin([
    'id' => 'itemsGrid',
    'timeout' => false,
    'enablePushState' => false,
]);
?>
    <style>
        .table {
            background-color: white;
        }
    </style>
    <div class="container-fluid">
        <?php
        $form = ActiveForm::begin([
            'id' => 'menu-item-search-form',
            'action' => ['item/index', 'id' => $_GET['id'], 'x' => $_GET['x']],
            'method' => 'get',
            'options' => ['data-pjax' => true]
        ]);
        ?>
        <div class="row pl-2">
            <div class="w-75">
                <?php try {
                    echo $form->field($searchModel, 'name')->textInput()->label(false);
                } catch (InvalidConfigException $e) {
                } ?>
            </div>
            <div class="pl-1 pb-3">
                <?= Html::submitButton('<span class="fas fa-search"></span>', ['class' => 'btn btn-success']); ?>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
        <div class="row">
            <div class="col-12">
                <?php
                if ($dataProvider->totalCount > 0) {
                    try {
                        echo GridView::widget([
                            'dataProvider' => $dataProvider,
                            'layout' => "{pager}{summary}{items}{pager}",
                            'options' => [
                                'class' => 'table-responsive',
                            ],
                            'responsiveWrap' => false,
                            'pager' => [
                                'firstPageLabel' => 'Inicio',
                                'lastPageLabel' => 'Fin'
                            ],
                            'columns' => [
                                [
                                    'class' => 'yii\grid\SerialColumn',
                                ],
                                [
                                    'attribute' => 'name',
                                    'label' => 'Nombre'
                                ],
                                [
                                    'attribute' => 'description',
                                    'label' => 'Descripción'
                                ],
                                [
                                    'attribute' => 'price',
                                    'label' => 'Precio',
                                    'value' => function ($data) {
                                        return isset($data['price']) ? '$ ' . $data['price'] : '';
                                    }
                                ],
                                [
                                    'attribute' => 'price_final',
                                    'label' => 'Precio final',
                                    'format' => 'html',
                                    'value' => function ($data) {
                                        return isset($data['price_final']) ? '<strong>$ ' . $data['price_final'] . '</strong>' : '';
                                    }
                                ],
                                [
                                    'attribute' => 'weighing',
                                    'label' => 'Ponderación'
                                ],
                                [
                                    'attribute' => 'state',
                                    'label' => 'Estado',
                                    'format' => 'html',
                                    'value' => function ($data) {
                                        return $data['state'] ? '<strong style="color:green">Activado</strong>' : '<strong style="color:tomato">Desactivado</strong>';
                                    }
                                ],
                                [
                                    'class' => 'yii\grid\ActionColumn',
                                    'template' => '{view} {update} {state} {delete}',
                                    'contentOptions' => ['style' => 'width:200px; white-space:nowrap;'],
                                    'header' => 'Acciones',
                                    'buttons' => [
                                        'view' => function ($url, $model) {
                                            return HTML::button('<i class="far fa-eye"></i>', ArrayHelper::merge(['value' => 'detail?id=' . $_GET['id'] . '&oid=' . $model['_id']->__toString() . '&x=' . $_GET['x']], ['class' => 'btn btn-info _modalButton'], ['title' => Yii::t('app', 'Detalle del producto')]));
                                        },
                                        'update' => function ($url, $model) {
                                            return Html::a('<i class="far fa-edit"></i>', ['update', 'id' => $_GET['id'], 'x' => $_GET['x'], 'oid' => $model['_id']->__toString()], ['class' => 'btn btn-primary', 'title' => Yii::t('app', 'Editar producto'), 'data-pjax' => 0]);
                                        },
                                        'state' => function ($url, $model) {
                                            if ($model['state'])
                                                return HTML::button('<i class="far fa-times-circle"></i>', ArrayHelper::merge(['value' => $model['_id']->__toString()], ['class' => 'btn btn-danger _disableState'], ['title' => Yii::t('app', 'Desactivar producto')]));
                                            return HTML::button('<i class="far fa-check-circle"></i>', ArrayHelper::merge(['value' => $model['_id']->__toString()], ['class' => 'btn btn-success _enableState'], ['title' => Yii::t('app', 'Activar producto')]));
                                        },
                                        'delete' => function ($url, $model) {
                                            return HTML::button('<i class="far fa-trash-alt"></i>', ArrayHelper::merge(['value' => $model['_id']->__toString()], ['class' => 'btn btn-secondary _delete'], ['title' => Yii::t('app', 'Eliminar producto')]));
                                        },
                                    ],
                                ],
                            ]

                        ]);
                    } catch (Exception $e) {
                    }
                } else {
                    ?>
                    <div class="empty"
                         style="padding: .75rem; border: 1px solid #dee2e6; background-color: rgba(0,0,0,.05);">No se
                        encontraron resultados.
                    </div>
                    <?php
                }
                ?>
            </div>
        </div>
    </div>
<?php

Pjax::end();

$urlDelete = Yii::$app->getUrlManager()->createAbsoluteUrl('item/delete');
$urlDisable = Yii::$app->getUrlManager()->createAbsoluteUrl('item/disable');
$urlEnable = Yii::$app->getUrlManager()->createAbsoluteUrl('item/enable');

$val = $_GET['id'];
$valOid = $_GET['x'];

$script = <<< JS
$(document).ready(function() {
    $('body').on('click','._delete', function(e){
        if (confirm('¿Desea ELIMINAR este item?'))
            var valX = $(this).val();
            var valId = '$val';
            var valOid = '$valOid';
            $.ajax({
                url: '$urlDelete',
                type: 'GET',
                data: {id: valId, oid: valOid, x: valX},
                dataType: 'json',
                success: function(){
                    $.pjax.reload({container: '#itemsGrid', timeout: false});            
                }
            });
        e.preventDefault();
    });
    $('body').on('click','._disableState', function(e){
        if (confirm('¿Desea DESACTIVAR este item?'))
            var valX = $(this).val();
            var valId = '$val';
            var valOid = '$valOid';
            $.ajax({
                url: '$urlDisable',
                type: 'GET',
                data: {id: valId, oid: valOid, x: valX},
                dataType: 'json',
                success: function(){
                    $.pjax.reload({container: '#itemsGrid', timeout: false});
                }
            });
        e.preventDefault();
    });
    $('body').on('click','._enableState', function(e){
        if (confirm('¿Desea ACTIVAR este item?'))
            var valX = $(this).val();
            var valId = '$val';
            var valOid = '$valOid';
            $.ajax({
                url: '$urlEnable',
                type: 'GET',
                data: {id: valId, oid: valOid, x: valX},
                dataType: 'json',
                success: function(){
                    $.pjax.reload({container: '#itemsGrid', timeout: false});
                }
            });
        e.preventDefault();
    });
});
JS;
$this->registerJs($script);