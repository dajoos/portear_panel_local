<?php

use yii\base\InvalidConfigException;

//quitar comentarios de las siguientes tres líneas cuando se implementen en producción

//$_SERVER['SERVER_PORT'] = 443;
//$_SERVER['HTTPS'] = 'on';
//if (isset($_SERVER['HTTP_X_REAL_IP'])) $_SERVER['REMOTE_ADDR'] = $_SERVER['HTTP_X_REAL_IP'];

// comente las siguientes dos líneas cuando se implementen en producción
defined('YII_DEBUG') or define('YII_DEBUG', true);
defined('YII_ENV') or define('YII_ENV', 'dev');

require __DIR__ . '/../vendor/autoload.php';
require __DIR__ . '/../vendor/yiisoft/yii2/Yii.php';

$config = require __DIR__ . '/../config/web.php';

try {
    (new yii\web\Application($config))->run();
} catch (InvalidConfigException $e) {
}
