<?php

/* @var $this yii\web\View */

/* @var $model ResetPasswordForm */

use app\models\ResetPasswordForm;
use yii\base\InvalidConfigException;
use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use yii\bootstrap4\Breadcrumbs;

$this->title = 'Restablecer la contraseña';
$this->params['breadcrumbs'][] = $this->title;

if (Yii::$app->user->isGuest){
?>
<div class="login-box">
    <?php
    }else{
    ?>
    <div class="container-fluid">
        <div class="row">
            <div class=" col-xl-6 col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <?php
                }
                if (Yii::$app->user->isGuest) {
                    try {
                        echo Breadcrumbs::widget([
                            'homeLink' => [
                                'label' => Yii::t('yii', 'Inicio'),
                                'url' => $this->params['home'] ?? Yii::$app->homeUrl
                            ],
                            'links' => $this->params['breadcrumbs'] ?? [],
                            'options' => [
                                'class' => 'float-sm-right'
                            ]
                        ]);
                    } catch (Exception $e) {
                    }
                }
                ?>
                <h1><?= (Yii::$app->user->isGuest) ? Html::encode($this->title) : '' ?></h1>

                <p>Elija su nueva contraseña:</p>

                <div class="w-100">
                    <?php $form = ActiveForm::begin(['id' => 'reset-password-form']); ?>

                    <?php try {
                        echo $form->field($model, 'password')->passwordInput(['autofocus' => true]);
                    } catch (InvalidConfigException $e) {
                    } ?>

                    <div class="form-group">
                        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
                    </div>

                    <?php ActiveForm::end(); ?>
                </div>
                <?php
                if (Yii::$app->user->isGuest){
                ?>
            </div>
            <?php
            } else {
            ?>
        </div>
    </div>
</div>
<?php
}
?>
