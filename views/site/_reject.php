<?php

use app\models\Items;
use app\models\Orders;
use kartik\grid\GridView;
use kartik\widgets\ActiveForm;
use yii\base\DynamicModel;
use yii\data\ArrayDataProvider;
use yii\helpers\Html;

/* @var $model DynamicModel */
/* @var $modelX Orders */
/* @var $dataProvider ArrayDataProvider */

$form = ActiveForm::begin([
    'id' => 'registerForm',
    'fieldConfig' => [
        'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
        'horizontalCssClasses' => [
            'label' => 'col-12',
            'wrapper' => 'col-12',
            'error' => '',
            'hint' => '',
        ],
    ],
]);
echo $form->field($model, 'id')->hiddenInput()->label(false);
echo $form->field($model, 'reject_reason')->label('Motivo del rechazo');
echo Html::submitButton('Rechazar pedido', ['class' => 'btn btn-danger']);
ActiveForm::end();

try {
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'layout' => "{items}",
        'options' => [
            'class' => 'table-responsive',
        ],
        'panel' => [
            'heading' => 'CÓDIGO: ' . $modelX['code'],
            'after' => false,
            'before' => false,
            'footer' => false,
        ],
        'summary' => false,
        'responsiveWrap' => false,
        'showPageSummary' => true,
        'id' => 'orderModal',
        'pjax' => true,
        'toolbar' => false,
        'columns' => Items::columnsSite($modelX),
    ]);
} catch (Exception $e) {
}

$url = Yii::$app->getUrlManager()->createAbsoluteUrl(['site/reject', 'id' => $model['id']]);
?>
<script>
    $(function () {
        $('#registerForm').submit(function (e) {
            $.ajax({
                url: '<?php echo $url; ?>',
                type: 'POST',
                dataType: 'json',
                data: new FormData(this),
                processData: false,
                contentType: false,
                success: function (data) {
                    console.log(data);
                    if (data.code === 1) {
                        $('.form-control').val(null);
                        $(document).find('#modal').modal('hide');
                        $.pjax.reload({container: '#ordersGrid-pjax', timeout: false});
                    }
                },
            });
            e.preventDefault();
        });
    });
</script>
