$(function () {
    $('body').on('click', '._modalButton', function () {
        $('#modal').modal('show')
            .find('#modalContent')
            .load($(this).attr('value'));
    });
});