<?php

use app\models\Locals;
use kartik\detail\DetailView;
use yii\bootstrap4\Modal;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/* @var $model Locals */

$this->title = $model['name'];

$this->params['breadcrumbs'] = [['label' => $this->title]];

?>
    <style>
        #map {
            height: 100%;
            width: 100%;
            min-height: 400px;
        }
    </style>
<?php
Modal::begin([
    'title' => '<h3>Imagen del local</h3>',
    'id' => 'modal',
    'size' => 'modal-sm'
]);
echo "<div class='clearfix' id='modalContent'>" . Html::img(Yii::getAlias('@web') . '/img/local_profile/' . $model['photo'], ['width' => '100%']) . "</div>";
Modal::end();

?>
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-12 pb-2">
                <?php
                echo HTML::button('<i class="fas fa-image"></i> Portada', ArrayHelper::merge(['class' => 'btn btn-primary _modalButton'], ['title' => Yii::t('app', 'Ver portada')]));
                echo '&nbsp';
                echo Html::a('<i class="fas fa-calendar-alt"></i> Horarios', ['local/schedule', 'id' => $_GET['id']], ['class' => 'btn btn-primary', 'title' => Yii::t('app', 'Ver horarios'),]);
                echo '&nbsp';
                echo Html::a('<i class="fas fa-edit"></i> Editar', ['local/update', 'id' => $_GET['id']], ['class' => 'btn btn-primary', 'title' => Yii::t('app', 'Actualizar información'),]);
                echo '&nbsp';
                echo Html::a('<i class="fas fa-list"></i> Productos', ['product/index', 'id' => $_GET['id']], ['class' => 'btn btn-primary', 'title' => Yii::t('app', 'Actualizar productos'),]);
                ?>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 pb-2 text-right">
                <?php
                if (!$model['state']) {
                    echo HTML::button('<i class="fas fa-lock-open"></i> Abrir', ArrayHelper::merge(['value' => $model['_id']], ['class' => 'btn btn-success _openButton'], ['title' => Yii::t('app', 'Abrir')]));
                } else {
                    echo HTML::button('<i class="fas fa-lock"></i> Cerrar', ArrayHelper::merge(['value' => $model['_id']], ['class' => 'btn btn-danger _closeButton'], ['title' => Yii::t('app', 'Cerrar')]));
                }
                ?>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-12">
                <?php try {
                    echo DetailView::widget([
                        'model' => $model,
                        'options' => ['class' => 'bg-white'],
                        'attributes' => [
                            'name',
                            'situation',
                            [
                                'attribute' => 'created_at',
                                'format' => 'html',
                                'value' => function () use ($model) {
                                    return date('Y-m-d H:i:s', $model['created_at']);
                                }
                            ],
                            'phone',
                            'email',
                            'website',
                            'city',
                            'description',
                            'type',
                            [
                                'attribute' => 'state',
                                'format' => 'html',
                                'value' => function () use ($model) {
                                    return $model['state'] ? '<strong style="color:#00aae4">ABIERTO (de acuerdo al horario)</strong>' : '<strong style="color:tomato">CERRADO</strong>';
                                }
                            ],
                            [
                                'attribute' => 'active',
                                'format' => 'html',
                                'value' => function () use ($model) {
                                    return $model['active'] ? '<strong style="color:green">SI</strong>' : '<strong style="color:tomato">NO</strong>';
                                }
                            ]
                        ],
                    ]);
                } catch (Exception $e) {
                }
                ?>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 bg-white p-0">
                <div id="map"></div>
            </div>
        </div>
    </div>
    <script>
        function initMap() {
            let uluru = {
                lat: <?= $model['location']['coordinates'][0] ?>,
                lng: <?= $model['location']['coordinates'][1] ?>
            };
            let map = new google.maps.Map(document.getElementById('map'), {zoom: 17, center: uluru});
            let marker = new google.maps.Marker({position: uluru, map: map});
        }
    </script>

    <script defer
            src="https://maps.googleapis.com/maps/api/js?key=<?= Yii::$app->params['google-map-api-key'] ?>&callback=initMap">
    </script>
<?php

$urlOpenOne = Yii::$app->getUrlManager()->createAbsoluteUrl('local/open');
$urlCloseOne = Yii::$app->getUrlManager()->createAbsoluteUrl('local/close');
$urlHome = Yii::$app->getUrlManager()->createAbsoluteUrl('local/index');
$script = <<< JS
$(document).ready(function() {
    $('body').on('click','._openButton', function(e){
        if (confirm('¿Desea ABRIR este local?'))
            var val = $(this).val();
            $.ajax({
                url: '$urlOpenOne',
                type: 'GET',
                data: {id: val},
                dataType: 'json',
                success: function(data){
                    if (data.code === 1){
                    console.log(data);
                        location.href = '$urlHome?id='.concat(val);
                    }
                }
            });
        e.preventDefault();
    });
    $('body').on('click','._closeButton', function(e){
        if (confirm('¿Desea CERRAR este local?'))
            var val = $(this).val();
            $.ajax({
                url: '$urlCloseOne',
                type: 'GET',
                data: {id: val},
                dataType: 'json',
                success: function(data){
                    if (data.code === 1){
                        location.href = '$urlHome?id='.concat(val);
                    }
                }
            });
        e.preventDefault();
    });
});
JS;
$this->registerJs($script);
?>