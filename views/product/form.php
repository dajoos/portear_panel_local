<?php

use app\models\Locals;
use app\models\Products;
use kartik\date\DatePicker;
use kartik\widgets\ActiveForm;
use kartik\number\NumberControl;
use kartik\select2\Select2;
use kartik\widgets\TimePicker;
use yii\base\InvalidConfigException;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/* @var $model Products */
/* @var $local Locals */

if (!isset($model['_id']))
    $this->title = 'Nuevo producto - ' . $local['name'];
else
    $this->title = 'Actualizar producto - ' . ($name ?? $model['name']);

$this->params['breadcrumbs'] = [['label' => 'Productos', 'url' => ['index', 'id' => $_GET['id']]], ['label' => $this->title]];

$this->params['home'] = ['local/index', 'id' => $_GET['id']];

$form = ActiveForm::begin(['id' => 'menu-product-new-form', 'enableClientValidation' => false]);

?>
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-5 col-md-5 col-sm-12">
            <?= $form->field($model, 'name'); ?>
        </div>
        <div class="col-lg-5 col-md-5 col-sm-12">
            <?= $form->field($model, 'description'); ?>
        </div>
        <div class="col-lg-2 col-md-2 col-sm-12">
            <?php try {
                echo $form->field($model, 'weighing')->widget(NumberControl::class, [
                ]);
            } catch (Exception $e) {
            } ?>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-3 col-md-3 col-sm-12">
            <?php try {
                echo $form->field($model, 'type')->widget(Select2::class, [
                    'data' => ArrayHelper::map(Products::getTypes(), 'type', 'type'),
                    'pluginOptions' => [
                        'allowClear' => true,
                        'multiple' => false,
                        'placeholder' => '',
                    ],
                    'pluginEvents' => [
                        'select2:select' => "function() {
                            validation();
                        }",
                    ],
                    'options' => [
                        'value' => !isset($model['_id']) ? 'Indefinido' : $model['type'],
                    ]
                ]);
            } catch (Exception $e) {
            } ?>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-12">
            <label class="has-star">Validación <span style="color: tomato">*</span></label>
            <div id="validation000">
                <div class="empty"
                     style="padding: 8px; border: 1px solid #dee2e6; background-color: rgba(0,0,0,.05);">Seleccione un
                    tipo de activación.
                </div>
            </div>
            <div id="validation001" style="display: none">
                <?php try {
                    echo $form->field($model, 'undefined')->textInput(['readonly' => true, 'class' => 'bg-white', 'value' => 'Activo'])->label(false);
                } catch (InvalidConfigException $e) {
                } ?>
            </div>
            <div id="validation002" style="display: none">
                <?php try {
                    echo $form->field($model, 'single_date')->widget(DatePicker::class, [
                        'removeButton' => false,
                        'pluginOptions' => [
                            'autoclose' => true,
                            'format' => 'yyyy-mm-dd',
                            'todayHighlight' => true
                        ],
                        'options' => [
                            'readonly' => true,
                            'class' => 'bg-white',
                        ]
                    ])->label(false);
                } catch (Exception $e) {
                } ?>
            </div>
            <div id="validation003" style="display: none">
                <div class="row">
                    <div class="col-6">
                        <?php try {
                            echo $form->field($model, 'start_date')->widget(DatePicker::class, [
                                'removeButton' => false,
                                'pluginOptions' => [
                                    'autoclose' => true,
                                    'format' => 'yyyy-mm-dd',
                                    'todayHighlight' => true
                                ],
                                'options' => [
                                    'readonly' => true,
                                    'class' => 'bg-white',
                                ]
                            ])->label(false);
                        } catch (Exception $e) {
                        } ?>
                    </div>
                    <div class="col-6">
                        <?php try {
                            echo $form->field($model, 'end_date')->widget(DatePicker::class, [
                                'removeButton' => false,
                                'pluginOptions' => [
                                    'autoclose' => true,
                                    'format' => 'yyyy-mm-dd',
                                    'todayHighlight' => true
                                ],
                                'options' => [
                                    'readonly' => true,
                                    'class' => 'bg-white',
                                ]
                            ])->label(false);
                        } catch (Exception $e) {
                        } ?>
                    </div>
                </div>
            </div>
            <div id="validation004" style="display: none">
                <?php
                $days = [];
                for ($i = 1; $i <= 30; $i++) {
                    array_push($days, ['day' => $i]);
                }
                ?>
                <?php try {
                    echo $form->field($model, 'day_month')->widget(Select2::class, [
                        'data' => ArrayHelper::map($days, 'day', 'day'),
                        'pluginOptions' => [
                            'allowClear' => true,
                            'multiple' => false,
                            'placeholder' => '',
                        ],
                    ])->label(false);
                } catch (Exception $e) {
                } ?>
            </div>
            <div id="validation005" style="display: none">
                <?php try {
                    echo $form->field($model, 'day_week')->widget(Select2::class, [
                        'data' => [
                            'sunday' => 'Domingo',
                            'monday' => 'Lunes',
                            'tuesday' => 'Martes',
                            'wednesday' => 'Miércoles',
                            'thursday' => 'Jueves',
                            'friday' => 'Viernes',
                            'saturday' => 'Sábado'
                        ],
                        'pluginOptions' => [
                            'allowClear' => true,
                            'multiple' => false,
                            'placeholder' => '',
                        ],
                    ])->label(false);
                } catch (Exception $e) {
                } ?>
            </div>
        </div>
        <div class="col-lg-2 col-md-2 col-sm-12">
            <?php try {
                echo $form->field($model, 'open_time')->widget(TimePicker::class, [
                    'options' => [
                        'readonly' => true,
                        'class' => 'bg-white',
                        'value' => !isset($model['_id']) ? '00:01' : $model['open_time'],
                    ],
                    'pluginOptions' => [
                        'showSeconds' => false,
                        'showMeridian' => false,
                        'minuteStep' => 1,
                        'defaultTime' => false,
                    ],
                ]);
            } catch (Exception $e) {
            } ?>
        </div>
        <div class="col-lg-2 col-md-2 col-sm-12">
            <?php try {
                echo $form->field($model, 'close_time')->widget(TimePicker::class, [
                    'options' => [
                        'readonly' => true,
                        'class' => 'bg-white',
                        'value' => !isset($model['_id']) ? '23:59' : $model['close_time'],
                    ],
                    'pluginOptions' => [
                        'showSeconds' => false,
                        'showMeridian' => false,
                        'minuteStep' => 1,
                        'defaultTime' => false,
                    ]
                ]);
            } catch (Exception $e) {
            } ?>
        </div>
        <div class="col-lg-2 col-md-2 col-sm-12">
            <?php try {
                echo $form->field($model, 'state')->widget(Select2::class, [
                    'data' => [1 => 'Activo', 0 => 'Inactivo'],
                    'pluginOptions' => [
                        'allowClear' => true,
                        'multiple' => false,
                        'placeholder' => '',
                    ],
                    'options' => [
                        'value' => !isset($model['_id']) ? 0 : $model['state'],
                    ]
                ]);
            } catch (Exception $e) {
            } ?>
        </div>
    </div>
    <div class="row">
        <div class="col-12 text-right">
            <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']); ?>
        </div>
    </div>

</div>
<?php ActiveForm::end(); ?>
<script>
    window.addEventListener("load", function () {
        validation();
    });

    function validation() {
        let option0 = document.getElementById('validation000');
        let option1 = document.getElementById('validation001');
        let option2 = document.getElementById('validation002');
        let option3 = document.getElementById('validation003');
        let option4 = document.getElementById('validation004');
        let option5 = document.getElementById('validation005');
        let principal = $('#products-type').val();
        switch (principal) {
            case 'Indefinido':
                option0.style.display = 'none';
                option1.style.display = 'block';
                option2.style.display = 'none';
                option3.style.display = 'none';
                option4.style.display = 'none';
                option5.style.display = 'none';
                break;
            case 'Fecha única':
                option0.style.display = 'none';
                option1.style.display = 'none';
                option2.style.display = 'block';
                option3.style.display = 'none';
                option4.style.display = 'none';
                option5.style.display = 'none';
                break;
            case 'Rango de fechas':
                option0.style.display = 'none';
                option1.style.display = 'none';
                option2.style.display = 'none';
                option3.style.display = 'block';
                option4.style.display = 'none';
                option5.style.display = 'none';
                break;
            case 'Día del mes':
                option0.style.display = 'none';
                option1.style.display = 'none';
                option2.style.display = 'none';
                option3.style.display = 'none';
                option4.style.display = 'block';
                option5.style.display = 'none';
                break;
            case 'Día de semana':
                option0.style.display = 'none';
                option1.style.display = 'none';
                option2.style.display = 'none';
                option3.style.display = 'none';
                option4.style.display = 'none';
                option5.style.display = 'block';
                break;
        }
    }
</script>
