<?php

namespace app\search;

use app\models\Items;
use app\models\Locals;
use app\models\Products;
use MongoDB\BSON\ObjectId;
use MongoDB\BSON\Regex;
use Yii;
use yii\base\Model;
use yii\data\ArrayDataProvider;

class ItemSearch extends Items
{
    public function rules(): array
    {
        return [
            [
                [
                    '_id',
                    'name',
                    'description',
                    'price',
                    'price_final',
                    'image',
                    'created_at',
                    'updated_at',
                    'deleted_at',
                    'state',
                    'delete',
                    'weighing',
                ], 'safe'],
        ];
    }

    public function scenarios(): array
    {
        return Model::scenarios();
    }

    public function search($params)
    {
        $search = $params["ItemSearch"]["name"] ?? '';

        if (!empty(Yii::$app->mongodb)) {
            $collection = Yii::$app->mongodb->getCollection(Locals::collectionName());
            $result = $collection->aggregate([
                [
                    '$match' => [
                        '_id' => $params['id']
                    ]
                ],
                [
                    '$unwind' => '$products'
                ],
                [
                    '$match' => [
                        'products._id' => new ObjectId($params['x']),
                    ]
                ],
                [
                    '$unwind' => '$products.items'
                ],
                [
                    '$match' => [
                        '$or' => [
                            [
                                'products.items.name' => new Regex(".*$search.*", 'i')
                            ],
                            [
                                'products.items.description' => new Regex(".*$search.*", 'i')
                            ]
                        ]
                    ]
                ],
                [
                    '$match' => [
                        'products.items.delete' => false
                    ]
                ],
                [
                    '$sort' => [
                        'products.items.state' => -1,
                        'products.items.weighing' => -1
                    ]
                ],
                [
                    '$group' => [
                        '_id' => '$_id',
                        'items' => [
                            '$push' => '$products.items'
                        ]
                    ]
                ],
                [
                    '$limit' => 1
                ],
            ]);

            $this->load($params);

            $query = new Products(array_shift($result));

            return new ArrayDataProvider([
                'allModels' => $query['items'],
                'pagination' => [
                    'pageSize' => 20
                ],
                'sort' => false,
            ]);
        }
        return [];

    }
}
