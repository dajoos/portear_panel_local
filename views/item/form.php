<?php

use app\models\Items;
use app\models\Locals;
use app\models\Products;
use kartik\file\FileInput;
use kartik\widgets\ActiveForm;
use kartik\number\NumberControl;
use kartik\select2\Select2;
use yii\helpers\Html;


/* @var $model Items */
/* @var $local Locals */
/* @var $product Products */

if (!isset($model['_id']))
    $this->title = 'Nuevo item - ' . $product['name'] . ' - ' . $local['name'];
else
    $this->title = 'Actualizar item - ' . $product['name'] . ' - ' . ($name ?? $model['name']);

$this->params['breadcrumbs'] = [['label' => 'Items', 'url' => ['index', 'id' => $_GET['id'], 'x' => $_GET['x']]], ['label' => $this->title]];

$this->params['home'] = ['index', 'id' => $_GET['id']];

$form = ActiveForm::begin(['id' => 'menu-item-new-form', 'enableClientValidation' => false]);

?>
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <?php try {
                echo $form->field($model, 'image')->widget(FileInput::class, [
                    'options' => [
                        'accept' => 'image/*',
                        'multiple' => false,
//                        'minWidth' => 500, 'maxWidth' => 500, 'minHeight' => 500, 'maxHeight' => 500
                    ],
                    'pluginOptions' => [
                        'showPreview' => true,
                        'showCaption' => true,
                        'showUpload' => false,
                        'showRemove' => false,
                        'initialPreview' => [
                            $model['image'] ? Html::img(Yii::getAlias('@web') . '/img/products/' . $model['image'], ['width' => '100%']) : null
                        ],
                        'overwriteInitial' => false,
                        'allowedFileExtensions' => ['png', 'jpg', 'jpeg'],
                    ]
                ]);
            } catch (Exception $e) {
            } ?>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-4 col-md-4 col-sm-12">
            <?= $form->field($model, 'name'); ?>
        </div>
        <div class="col-lg-8 col-md-8 col-sm-12">
            <?= $form->field($model, 'description'); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-4 col-md-4 col-sm-12">
            <?php try {
                echo $form->field($model, 'weighing')->widget(NumberControl::class, [
                ]);
            } catch (Exception $e) {
            } ?>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-12">
            <?php try {
                echo $form->field($model, 'price')->widget(NumberControl::class, [
                    'maskedInputOptions' => [
                        'prefix' => '$ '
                    ],
                ]);
            } catch (Exception $e) {
            } ?>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-12">
            <?php try {
                echo $form->field($model, 'state')->widget(Select2::class, [
                    'data' => [1 => 'Activo', 0 => 'Inactivo'],
                    'pluginOptions' => [
                        'allowClear' => true,
                        'multiple' => false,
                        'placeholder' => '',
                    ],
                    'options' => [
                        'value' => !isset($model['_id']) ? 0 : $model['state'],
                    ]
                ]);
            } catch (Exception $e) {
            } ?>
        </div>
    </div>
    <div class="row">
        <div class="col-12 text-right">
            <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']); ?>
        </div>
    </div>

</div>
<?php ActiveForm::end(); ?>
