<?php

namespace app\controllers;

use app\models\Schedule;
use app\models\Locals;
use app\models\User;
use Gumlet\ImageResize;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\UploadedFile;

class LocalController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors(): array
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => [
                    'index',
                    'schedule',
                    'update',
                    'open',
                    'close',
                ],
                'rules' => [
                    [
                        'actions' => [
                            'index',
                            'schedule',
                            'update',
                            'open',
                            'close',
                        ],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function () {
                            return User::isOwner();
                        },
                    ],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions(): array
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * @return string
     */
    public function actionIndex(): string
    {
        $model = Locals::findById($_GET['id']);
        return $this->render('index', ['model' => $model]);
    }

    public function actionOpen()
    {
        Locals::updateAll(['state' => true], ['_id' => $_GET['id']]);
        return json_encode(['code' => 1]);
    }

    public function actionClose()
    {
        Locals::updateAll(['state' => false], ['_id' => $_GET['id']]);
        return json_encode(['code' => 1]);
    }

    public function actionSchedule()
    {
        $model = Locals::findById($_GET['id']);
        $dynamicModel = new Schedule([
            'monday_open' => isset($model['schedule']['monday']['start']) ? gmdate('H:i', $model['schedule']['monday']['start'] * 60) : '',
            'monday_close' => isset($model['schedule']['monday']['end']) ? gmdate('H:i', $model['schedule']['monday']['end'] * 60) : '',
            'tuesday_open' => isset($model['schedule']['tuesday']['start']) ? gmdate('H:i', $model['schedule']['tuesday']['start'] * 60) : '',
            'tuesday_close' => isset($model['schedule']['tuesday']['end']) ? gmdate('H:i', $model['schedule']['tuesday']['end'] * 60) : '',
            'wednesday_open' => isset($model['schedule']['wednesday']['start']) ? gmdate('H:i', $model['schedule']['wednesday']['start'] * 60) : '',
            'wednesday_close' => isset($model['schedule']['wednesday']['end']) ? gmdate('H:i', $model['schedule']['wednesday']['end'] * 60) : '',
            'thursday_open' => isset($model['schedule']['thursday']['start']) ? gmdate('H:i', $model['schedule']['thursday']['start'] * 60) : '',
            'thursday_close' => isset($model['schedule']['thursday']['end']) ? gmdate('H:i', $model['schedule']['thursday']['end'] * 60) : '',
            'friday_open' => isset($model['schedule']['friday']['start']) ? gmdate('H:i', $model['schedule']['friday']['start'] * 60) : '',
            'friday_close' => isset($model['schedule']['friday']['end']) ? gmdate('H:i', $model['schedule']['friday']['end'] * 60) : '',
            'saturday_open' => isset($model['schedule']['saturday']['start']) ? gmdate('H:i', $model['schedule']['saturday']['start'] * 60) : '',
            'saturday_close' => isset($model['schedule']['saturday']['end']) ? gmdate('H:i', $model['schedule']['saturday']['end'] * 60) : '',
            'sunday_open' => isset($model['schedule']['sunday']['start']) ? gmdate('H:i', $model['schedule']['sunday']['start'] * 60) : '',
            'sunday_close' => isset($model['schedule']['sunday']['end']) ? gmdate('H:i', $model['schedule']['sunday']['end'] * 60) : ''
        ]);
        if ($dynamicModel->load(Yii::$app->request->post()) && $dynamicModel->validate()) {
            $schedule = [];
            if (strlen($_POST['Schedule']['monday_open']) > 0 && strlen($_POST['Schedule']['monday_close']) > 0) {
                $time = [
                    'start' => ((date('H', strtotime($_POST['Schedule']['monday_open'])) * 60) + (date('i', strtotime($_POST['Schedule']['monday_open'])))),
                    'end' => ((date('H', strtotime($_POST['Schedule']['monday_close'])) * 60) + (date('i', strtotime($_POST['Schedule']['monday_close']))))
                ];
                $schedule['monday'] = $time;
            }
            if (strlen($_POST['Schedule']['tuesday_open']) > 0 && strlen($_POST['Schedule']['tuesday_close']) > 0) {
                $time = [
                    'start' => ((date('H', strtotime($_POST['Schedule']['tuesday_open'])) * 60) + (date('i', strtotime($_POST['Schedule']['tuesday_open'])))),
                    'end' => ((date('H', strtotime($_POST['Schedule']['tuesday_close'])) * 60) + (date('i', strtotime($_POST['Schedule']['tuesday_close']))))
                ];
                $schedule['tuesday'] = $time;
            }
            if (strlen($_POST['Schedule']['wednesday_open']) > 0 && strlen($_POST['Schedule']['wednesday_close']) > 0) {
                $time = [
                    'start' => ((date('H', strtotime($_POST['Schedule']['wednesday_open'])) * 60) + (date('i', strtotime($_POST['Schedule']['wednesday_open'])))),
                    'end' => ((date('H', strtotime($_POST['Schedule']['wednesday_close'])) * 60) + (date('i', strtotime($_POST['Schedule']['wednesday_close']))))
                ];
                $schedule['wednesday'] = $time;
            }
            if (strlen($_POST['Schedule']['thursday_open']) > 0 && strlen($_POST['Schedule']['thursday_close']) > 0) {
                $time = [
                    'start' => ((date('H', strtotime($_POST['Schedule']['thursday_open'])) * 60) + (date('i', strtotime($_POST['Schedule']['thursday_open'])))),
                    'end' => ((date('H', strtotime($_POST['Schedule']['thursday_close'])) * 60) + (date('i', strtotime($_POST['Schedule']['thursday_close']))))
                ];
                $schedule['thursday'] = $time;
            }
            if (strlen($_POST['Schedule']['friday_open']) > 0 && strlen($_POST['Schedule']['friday_close']) > 0) {
                $time = [
                    'start' => ((date('H', strtotime($_POST['Schedule']['friday_open'])) * 60) + (date('i', strtotime($_POST['Schedule']['friday_open'])))),
                    'end' => ((date('H', strtotime($_POST['Schedule']['friday_close'])) * 60) + (date('i', strtotime($_POST['Schedule']['friday_close']))))
                ];
                $schedule['friday'] = $time;
            }
            if (strlen($_POST['Schedule']['saturday_open']) > 0 && strlen($_POST['Schedule']['saturday_close']) > 0) {
                $time = [
                    'start' => ((date('H', strtotime($_POST['Schedule']['saturday_open'])) * 60) + (date('i', strtotime($_POST['Schedule']['saturday_open'])))),
                    'end' => ((date('H', strtotime($_POST['Schedule']['saturday_close'])) * 60) + (date('i', strtotime($_POST['Schedule']['saturday_close']))))
                ];
                $schedule['saturday'] = $time;
            }
            if (strlen($_POST['Schedule']['sunday_open']) > 0 && strlen($_POST['Schedule']['sunday_close']) > 0) {
                $time = [
                    'start' => ((date('H', strtotime($_POST['Schedule']['sunday_open'])) * 60) + (date('i', strtotime($_POST['Schedule']['sunday_open'])))),
                    'end' => ((date('H', strtotime($_POST['Schedule']['sunday_close'])) * 60) + (date('i', strtotime($_POST['Schedule']['sunday_close']))))
                ];
                $schedule['sunday'] = $time;
            }
            $modelUpdate = Locals::findByIdUpdate($_GET['id']);
            $modelUpdate->schedule = $schedule;
            if ($modelUpdate->save()) {
                return $this->redirect(['index', 'id' => $_GET['id']]);
            }
        }
        return $this->render('schedule', ['model' => $model, 'dynamicModel' => $dynamicModel]);
    }

    /**
     * @return string
     * @throws \Gumlet\ImageResizeException
     */
    public function actionUpdate(): string
    {
        $model = Locals::findByIdUpdate($_GET['id']);
        $name = $model['name'];
        $city = $model['city'];
        if ($model->load(Yii::$app->request->post())) {
            $location['type'] = 'Point';
            $location['coordinates'] = array_map('floatval', $_POST['Locals']['location']['coordinates']);
            $model->location = $location;
            if ($model->validate()) {
                $coverPage = UploadedFile::getInstance($model, 'photo');
                if ($coverPage != null) {
                    $img = 'local_' . time();
                    $path = '../web/img/local_profile/' . $img . '.' . $coverPage->extension;
                    $model->photo = $img . '.' . $coverPage->extension;
                } else {
                    $back = Locals::findByIdUpdate($_GET['id']);
                    $model->photo = $back['photo'];
                }
                $model->name = $name;
                $model->city = $city;
                $model->updated_at = time();
                if ($model->save()) {
                    if (isset($path)) {
                        $coverPage->saveAs($path);
                        $image = new ImageResize($path);
                        $image->crop(800, 500, true, ImageResize::CROPCENTER);
                        $image->save($path);
                    }
                    return $this->render('index', ['model' => $model]);
                }
            }
        }
        return $this->render('form', ['model' => $model]);
    }

}
