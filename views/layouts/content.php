<?php
/* @var $content string */

use yii\bootstrap4\Breadcrumbs;
use yii\helpers\Html;
use yii\helpers\Inflector;

?>
<div class="content-wrapper">
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">
                        <?php
                        if (!is_null($this->title)) {
                            echo Html::encode($this->title);
                        } else {
                            echo Inflector::camelize($this->context->id);
                        }
                        ?>
                    </h1>
                </div>
                <div class="col-sm-6">
                    <?php
                    try {
                        echo Breadcrumbs::widget([
                            'homeLink' => [
                                'label' => Yii::t('yii', 'Inicio'),
                                'url' => $this->params['home'] ?? Yii::$app->homeUrl
                            ],
                            'links' => $this->params['breadcrumbs'] ?? [],
                            'options' => [
                                'class' => 'float-sm-right'
                            ]
                        ]);
                    } catch (Exception $e) {
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>

    <div class="content">
        <?= $this->render('principal', ['content' => $content]) ?>
    </div>
</div>