<?php

/* @var $this yii\web\View */

/* @var $model User */

use app\models\User;
use kartik\widgets\ActiveForm;
use yii\base\InvalidConfigException;
use yii\helpers\Html;

$this->title = 'Perfil';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="container-fluid">
    <div class="row">
        <div class=" col-xl-6 col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <?php $form = ActiveForm::begin(['options' => ['autocomplete' => 'off']]); ?>
            <?= $form->field($model, 'fullname') ?>
            <?= $form->field($model, 'email') ?>
            <?= $form->field($model, 'username') ?>
            <?= $form->field($model, 'phone') ?>
            <hr>
            <?php try {
                echo $form->field($model, 'password_aux')->passwordInput(['autocomplete' => 'off']);
            } catch (InvalidConfigException $e) {
            } ?>
            <div class="form-group">
                <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
