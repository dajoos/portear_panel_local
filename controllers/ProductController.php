<?php

namespace app\controllers;

use app\models\Products;
use app\models\Locals;
use app\models\User;
use app\search\ProductSearch;
use MongoDB\BSON\ObjectId;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;

class ProductController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors(): array
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => [
                    'index',
                    'new',
                    'detail',
                    'update',
                    'delete',
                    'disable',
                    'enable',

                ],
                'rules' => [
                    [
                        'actions' => [
                            'index',
                            'new',
                            'detail',
                            'update',
                            'delete',
                            'disable',
                            'enable',
                        ],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function () {
                            return User::isOwner();
                        },
                    ],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions(): array
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    private static function newAndUpdate($model, $type): array
    {
        $validation = false;
        $model->items = [];
        $model->updated_at = time();
        $model->open_time = ((date('H', strtotime($_POST['Products']['open_time'])) * 60) + (date('i', strtotime($_POST['Products']['open_time']))));
        $model->close_time = ((date('H', strtotime($_POST['Products']['close_time'])) * 60) + (date('i', strtotime($_POST['Products']['close_time']))));
        $model->delete = false;
        if ($model->validate()) {
            $model->_id = new ObjectID();
            $model->state = ($_POST['Products']['state'] == 1);
            $model->weighing = (int)$model['weighing'];
            $model->undefined = isset($model['undefined']);
            $model->single_date = $model['single_date'] ?? '';
            $model->start_date = $model['start_date'] ?? '';
            $model->end_date = $model['end_date'] ?? '';
            $model->day_month = isset($model['day_month']) ? (int)$model['day_month'] : null;
            $model->day_week = $model['day_week'] ?? '';
            switch ($type) {
                case 'new':
                    $model->created_at = time();
                    if (Products::newProduct($model)) {
                        $validation = true;
                    }
                    break;
                case 'update':
                    $update = Products::updateProduct($model);
                    if ($update) {
                        $validation = true;
                    } elseif ($update == 0) {
                        $validation = true;
                    }
                    break;
            }
        }
        $model->open_time = gmdate('H:i', $model['open_time'] * 60);
        $model->close_time = gmdate('H:i', $model['close_time'] * 60);
        $model->state = $model['state'] ? 1 : 0;
        return ['model' => $model, 'validation' => $validation];
    }

    public function actionDelete()
    {
        Products::deleteProduct();
        return json_encode(['code' => 1]);
    }

    public function actionDisable()
    {
        Products::stateProduct(false);
        return json_encode(['code' => 1]);
    }

    public function actionEnable()
    {
        Products::stateProduct(true);
        return json_encode(['code' => 1]);
    }

    /**
     * @return string
     */
    public function actionIndex(): string
    {
        $model = Locals::findByIdUpdate($_GET['id']);
        $searchModel = new ProductSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', ['searchModel' => $searchModel, 'dataProvider' => $dataProvider, 'model' => $model]);
    }

    public function actionNew()
    {
        $local = Locals::findByIdUpdate($_GET['id']);
        $model = new Products();
        if ($model->load(Yii::$app->request->post())) {
            $result = $this->newAndUpdate($model, 'new');
            if ($result['validation']) {
                Yii::$app->session->setFlash('success', $_POST['Products']['name'] . ' ingresado exitosamente ');
                return $this->redirect(['index', 'id' => $_GET['id']]);
            } else {
                $model = $result['model'];
            }
        }
        return $this->render('form', ['model' => $model, 'local' => $local]);
    }

    /**
     * @return string
     */
    public function actionDetail(): string
    {
        $model = Products::findByIdOid($_GET['id'], $_GET['x']);
        return $this->renderAjax('detail', ['model' => $model]);
    }

    public function actionUpdate()
    {
        $local = Locals::findByIdUpdate($_GET['id']);
        $model = Products::findByIdOid($_GET['id'], $_GET['x']);
        $name = $model['name'];
        $model->open_time = gmdate('H:i', $model['open_time'] * 60);
        $model->close_time = gmdate('H:i', $model['close_time'] * 60);
        $model->state = $model['state'] ? 1 : 0;
        if ($model->load(Yii::$app->request->post())) {
            $result = $this->newAndUpdate($model, 'update');
            if ($result['validation']) {
                Yii::$app->session->setFlash('info', $_POST['Products']['name'] . ' actualizado exitosamente ');
                return $this->redirect(['index', 'id' => $_GET['id']]);
            } else {
                $model = $result['model'];
            }
        }
        return $this->render('form', ['model' => $model, 'local' => $local, 'name' => $name]);
    }
}
