<?php

namespace app\search;

use app\models\Locals;
use app\models\Products;
use MongoDB\BSON\Regex;
use Yii;
use yii\base\Model;
use yii\data\ArrayDataProvider;

class ProductSearch extends Products
{
    public function rules(): array
    {
        return [
            [
                [
                    '_id',
                    'name',
                    'description',
                    'items',
                    'created_at',
                    'updated_at',
                    'deleted_at',
                    'type',
                    'weighing',
                    'state',
                    'open_time',
                    'close_time',
                    'delete',
                    'active',
                ], 'safe'],
        ];
    }

    public function scenarios(): array
    {
        return Model::scenarios();
    }

    public function search($params)
    {
        $h = date('H') * 60;
        $m = date('i');
        $now = $h + $m;

        $search = $params["ProductSearch"]["name"] ?? '';

        if (!empty(Yii::$app->mongodb)) {
            $collection = Yii::$app->mongodb->getCollection(Locals::collectionName());
            $result = $collection->aggregate([
                [
                    '$match' => [
                        '_id' => $params['id']
                    ]
                ],
                [
                    '$unwind' => '$products'
                ],
                [
                    '$match' => [
                        '$or' => [
                            [
                                'products.name' => new Regex(".*$search.*")
                            ],
                            [
                                'products.description' => new Regex(".*$search.*")
                            ]
                        ]
                    ]
                ],
                [
                    '$match' => [
                        'products.delete' => false
                    ]
                ],
                [
                    '$project' => [
                        '_id' => 1,
                        'products._id' => 1,
                        'products.name' => 1,
                        'products.description' => 1,
                        'products.created_at' => 1,
                        'products.updated_at' => 1,
                        'products.type' => 1,
                        'products.active' => Products::condActive($now),
                        'products.state' => 1,
                        'products.weighing' => 1,
                        'products.start_date' => 1,
                        'products.end_date' => 1,
                        'products.undefined' => 1,
                        'products.single_date' => 1,
                        'products.day_month' => 1,
                        'products.day_week' => 1,
                        'products.open_time' => 1,
                        'products.close_time' => 1,
                    ]
                ],
                [
                    '$sort' => [
                        'products.state' => -1,
                        'products.active' => -1,
                        'products.weighing' => -1
                    ]
                ],
                [
                    '$group' => [
                        '_id' => '$_id',
                        'products' => [
                            '$push' => '$products'
                        ]
                    ]
                ],
                [
                    '$limit' => 1
                ],
            ]);

            $this->load($params);

            $query = new Locals (array_shift($result));

            return new ArrayDataProvider([
                'allModels' => $query['products'],
                'pagination' => [
                    'pageSize' => 20
                ],
                'sort' => false,
            ]);
        }
        return [];

    }
}
