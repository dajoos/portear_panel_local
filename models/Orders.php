<?php

namespace app\models;

use Yii;
use yii\mongodb\ActiveRecord;

class Orders extends ActiveRecord
{
    public static function collectionName(): string
    {
        return 'orders';
    }

    public function attributes(): array
    {
        return [
            '_id',
            'code',
            'lat',
            'lon',
            'address',
            'reference',
            'total',
            'local',
            'created_date',
            'updated_date',
            'commission',
            'delivery_boy',
            'subtotal',
            'so',
            'surcharge',
            'version',
            'distance',
            'local_id',
            'firebase_token',
            'phone',
            'city',
            'user_id',
            'created_at',
            'updated_at',
            'payment',
            'personalized',
            'status_date',
            'items',
            'status',
        ];
    }

    public static function findByIdUpdate($id)
    {
        return self::find()->where(['_id' => $id])->one();
    }

    public static function updateStatus($id, $status, $reject, $cash_back): int
    {
        if (!empty(Yii::$app->mongodb)) {
            $collection = Yii::$app->mongodb->getCollection(self::collectionName());
            if ($reject != null) {
                return $collection->update(
                    [
                        '_id' => $id,
                        'status' => 0
                    ],
                    [
                        '$set' => [
                            'status' => $status,
                            'reject_reason' => $reject,
                            'cash_back' => $cash_back,
                            'updated_at' => time(),
                        ],
                        '$push' => [
                            'status_date' => [
                                'status' => $status,
                                'created_at' => time(),
                            ]
                        ]
                    ]
                );
            }
            return $collection->update(
                [
                    '_id' => $id
                ],
                [
                    '$set' => [
                        'status' => $status,
                        'updated_at' => time(),
                    ],
                    '$push' => [
                        'status_date' => [
                            'status' => $status,
                            'created_at' => time(),
                        ]
                    ]
                ]
            );
        }
        return 0;
    }

    public static function updateDeliveryBoyAssign($id, $status): int
    {
        if (!empty(Yii::$app->mongodb)) {
            $collection = Yii::$app->mongodb->getCollection(self::collectionName());
            return $collection->update(
                [
                    '_id' => $id,
                    'status' => 1
                ],
                [
                    '$set' => [
                        'status' => $status,
                        'updated_at' => time(),
                    ],
                    '$push' => [
                        'status_date' => [
                            'status' => $status,
                            'created_at' => time(),
                        ]
                    ]
                ]
            );
        }
        return 0;
    }

    public static function getNewOrders()
    {
        if (!User::getLocalsOwner()) {
            return [];
        }
        if (count(User::getLocalsOwner()['locals']) == 0) {
            return [];
        }
        $locals = [];
        foreach (User::getLocalsOwner()['locals'] as $array) {
            $locals[] = $array['local'];
        }

        return self::find()
            ->where(['in', 'local_id', $locals])
            ->andWhere(['status' => -1])
            ->andWhere(['or', ['payment.method' => 'Efectivo'], ['payment.paid_out' => true]])
            ->asArray()
            ->all();
    }

    public static function updateNotification()
    {
        if (!User::getLocalsOwner()) {
            return [];
        }
        if (count(User::getLocalsOwner()['locals']) == 0) {
            return [];
        }
        $locals = [];
        foreach (User::getLocalsOwner()['locals'] as $array) {
            $locals[] = $array['local'];
        }

        return self::updateAll(['status' => 0], ['and', ['in', 'local_id', $locals], ['status' => -1]]);
    }

    public static function isOwnerOrder()
    {
        if (isset($_POST['id'])) {
            $id = $_POST['id'];
        } elseif (isset($_POST['dynamic']['id'])) {
            $id = $_POST['dynamic']['id'];
        } elseif (isset($_GET['id'])) {
            $id = $_GET['id'];
        } else {
            return false;
        }
        if (!User::getLocalsOwner()) {
            return [];
        }
        if (count(User::getLocalsOwner()['locals']) == 0) {
            return [];
        }
        $locals = [];
        foreach (User::getLocalsOwner()['locals'] as $array) {
            $locals[] = $array['local'];
        }
        return self::find()
            ->where(['_id' => $id])
            ->andWhere(['in', 'local_id', $locals])
            ->one();
    }
}