<?php

namespace app\models;

use Yii;
use yii\base\Exception;
use yii\mongodb\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * User model
 *
 * @property string $_id
 * @property string $fullname
 * @property string $username
 * @property string $password
 * @property string $email
 * @property string $phone
 * @property string $authKey
 * @property string $accessToken
 * @property string $password_reset_token
 * @property string $locals
 * @property string $active
 * @property string $timestamp
 */
class User extends ActiveRecord implements IdentityInterface
{

    const STATUS_ACTIVE = true;
    public $password_aux;

    /**
     * {@inheritdoc}
     */
    public static function collectionName()
    {
        return 'premises_owners';
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return [
            [['fullname', 'username', 'email', 'phone'], 'string', 'max' => 255],
            [['fullname', 'username', 'email', 'phone'], 'string', 'min' => 10],
            [['username', 'email'], 'unique'],
            [['email'], 'email'],
            [['password'], 'string', 'min' => 10],
            [['authKey', 'accessToken', 'password'], 'string', 'max' => 255],
            [['username', 'password', 'active'], 'required'],
            [['fullname', 'username', 'email', 'password', 'phone', 'password_aux'], 'required', 'on' => 'update']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return [
            '_id' => 'id',
            'fullname' => 'Nombre',
            'username' => 'Usuario',
            'email' => 'Email',
            'password' => 'Contraseña',
            'locals' => 'Locales',
            'active' => 'Estado',
            'phone' => 'Teléfono',
            'password_aux' => 'Contraseña'
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributes(): array
    {
        return [
            '_id',
            'fullname',
            'username',
            'password',
            'phone',
            'email',
            'timestamp',
            'locals',
            'authKey',
            'accessToken',
            'active',
            'password_reset_token',
        ];
    }

    /**
     * Finds an identity by the given ID.
     * @param string|int $id the ID to be looked for
     * @return IdentityInterface|null the identity object that matches the given ID.
     * Null should be returned if such an identity cannot be found
     * or the identity is not in an active state (disabled, deleted, etc.)
     */
    public static function findIdentity($id): ?IdentityInterface
    {
        return self::findOne($id);
    }

    /**
     * Finds an identity by the given token.
     * @param mixed $token the token to be looked for
     * @param mixed $type the type of the token. The value of this parameter depends on the implementation.
     * For example, [[\yii\filters\auth\HttpBearerAuth]] will set this parameter to be `yii\filters\auth\HttpBearerAuth`.
     * @return IdentityInterface|null the identity object that matches the given token.
     * Null should be returned if such an identity cannot be found
     * or the identity is not in an active state (disabled, deleted, etc.)
     */
    public static function findIdentityByAccessToken($token, $type = null): ?IdentityInterface
    {
        return self::findOne(['accessToken' => $token]);
    }

    public static function findByUsername($username): ?User
    {
        return self::findOne(['username' => $username, 'active' => true]);
    }

    /**
     * Returns an ID that can uniquely identify a user identity.
     * @return string an ID that uniquely identifies a user identity.
     */
    public function getId(): string
    {
        return $this->_id;
    }

    /**
     * Returns a key that can be used to check the validity of a given identity ID.
     *
     * The key should be unique for each individual user, and should be persistent
     * so that it can be used to check the validity of the user identity.
     *
     * The space of such keys should be big enough to defeat potential identity attacks.
     *
     * This is required if [[User::enableAutoLogin]] is enabled. The returned key will be stored on the
     * client side as a cookie and will be used to authenticate user even if PHP session has been expired.
     *
     * Make sure to invalidate earlier issued authKeys when you implement force user logout, password change and
     * other scenarios, that require forceful access revocation for old sessions.
     *
     * @return string a key that is used to check the validity of a given identity ID.
     * @see validateAuthKey()
     */
    public function getAuthKey(): string
    {
        return $this->authKey;
    }

    /**
     * Validates the given auth key.
     *
     * This is required if [[User::enableAutoLogin]] is enabled.
     * @param $authKey
     * @return bool whether the given auth key is valid.
     * @see getAuthKey()
     */
    public function validateAuthKey($authKey): bool
    {
        return $this->authKey === $authKey;
    }

    public function validatePassword($password): bool
    {
        return password_verify($password, $this->password);
    }

    public static function isOwner(): bool
    {
        if (self::findOne(['_id' => Yii::$app->user->identity->_id, 'active' => true, 'locals.local' => $_GET['id']])) {
            return true;
        } else {
            return false;
        }
    }

    public static function getLocalsOwner(): ?User
    {
        return self::findOne(['_id' => Yii::$app->user->identity->_id, 'active' => true]);

    }

    public static function isPasswordResetTokenValid($token): bool
    {
        if (empty($token)) {
            return false;
        }
        $timestamp = (int)substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user']['passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    public function generatePasswordResetToken()
    {
        try {
            $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
        } catch (Exception $e) {
            $this->password_reset_token = false;
        }
    }

    public static function findByPasswordResetToken($token): ?User
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'active' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword(string $password)
    {
        $this->password = password_hash($password, PASSWORD_ARGON2I);
    }

    /**
     * Generates "remember me" authentication key
     * @throws \Exception
     */
    public function generateAuthKey()
    {
        $this->authKey = md5(random_bytes(5));
    }

    /**
     * Removes password reset token
     * @throws \Exception
     */
    public function removePasswordResetToken()
    {
        $this->accessToken = password_hash(random_bytes(10), PASSWORD_DEFAULT);
        $this->password_reset_token = null;
    }
}
